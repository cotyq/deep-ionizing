import argparse
import textwrap

import torch.optim as optim

from dataset_classes import *
from models import *
from torchiotools import patch_inference_tio, data_loading_tio
from transforms import *
from torch.utils.tensorboard import SummaryWriter
from ProblemHandler import *


class ModelLoader:
    def __init__(self, cfg_file=None):
        """ Constructor of modelLoader class. Read the cfg file and initialize the required variables training or
        testing a model.
        If the trainVal flag is True, it starts training the model.
        If the test flag is True, it makes the predictions for the test dataset from the trained model (wich could be
        trained right before or in previous moment).

        :param cfg_file: path of the cfg file with the parameters.
        """
        if cfg_file is None:  # Enter all the parameters manually
            print("No configuration file provided.")

        self.params = {  # Initialize required params by defalut
            'name': None,  # Trained model name
            'device': None,  # gpu/cpu
            'n_epochs': None,  # number of epochs
            'batch_size': None,
            'model_class': None,  # Model class name (located in models.py)
            'acnn_path': None,  # ACNN traned model path for regularization (if needed).
            'acnn_lambda': None,  # Weight used for the ACNN loss.
            'dice_lambda': None,  # Weight used for the Dice loss.
            'ce_lambda': None,  # Weight used for the Cross Entropy loss.
            'train_files_csv': None,  # Datasets csv
            'validation_files_csv': None,
            'test_files_csv': None,
            'models_folder': None,  # Location of the trained models
            'plots_folder': None,  # Location of the plots
            'plot_update_interval': None,  # Frequency of plots update (in epochs)
            'save_dice_plots': None,  # Save Dice coefficient plot over the epochs.
            'autosave_epochs': None,  # Frequency of model checkpoint saving.
            'resume_training': None,  # Continue training the model located in this path
            'optimizer': None,  # Optimizer used for training
            'learning_rate': None,
            'momentum': None,
            'weight_decay': None,
            'train_flag': None,
            'test_flag': None,
            'use_patch_training': None,
            'use_patch_inference': None,
            'load_segmentation': None,  # In patch image loading
            'transform_class': None  # Transforms that will applied to train/val splits.
        }

        self.params = cmf.set_cfg_params(cfg_file, self.params)

        self.problem_handler = eval(self.params['problem_handler'])()

        self.models = {
            'main': None,  # Trained model
            'acnn': None  # ACNN model used for regularization (if neccesary)
        }

        self.data = {
            'train_dataloader': None,
            'validation_dataset': None,
            'validation_dataloader': None,
            'test_dataset': None,
            'test_dataloader': None,
        }

        # Now set the previous variables to the values set in the cfg file
        self.cfg_path = cfg_file

        self.out_model_path = os.path.join(self.params['models_folder'], '{}.pt'.format(self.params['name']))

        self.params['device'] = torch.device("cuda" if torch.cuda.is_available() else "cpu") if str(
            self.params['device']) == 'cuda' else torch.device("cpu")

        self.load_datasets()  # Load the datasets provided in the csv files

        self.current_epoch = self.current_train_iteration = 0

        self.pt_loss = []  # Losses used by PyTorch

        self.losses_and_metrics = {}  # Losses and metrics stored as lists of floats for displaying in Tensorboard

        self.writer = SummaryWriter()

        if self.params['train_flag'] is True:
            self.train()

        # Test the trained model with the test data.
        if self.params['test_flag'] is True:
            self.test(torchio_pred=self.params['use_patch_inference'])

    def load_datasets(self):
        dataset_c_train = self.problem_handler.train_dataset_class
        transforms_c = eval(self.params['transform_class']) if self.params['transform_class'] else None  # TODO REMOVE

        pin_mem = False if str(self.params['device']) == 'cpu' else True  # Copy Tensors into CUDA pinned memory first.

        if self.params['train_flag']:
            if self.params['use_patch_training']:
                self.data['train_dataloader'] = data_loading_tio(self.params['train_files_csv'],
                                                                 self.params['training_patch_size'],
                                                                 self.params['max_queue_length'],
                                                                 self.params['samples_per_volume'],
                                                                 self.params['batch_size'],
                                                                 self.params['n_workers'],
                                                                 pin_mem,
                                                                 transforms_c,
                                                                 self.params['load_segmentation'])

                self.data['validation_dataloader'] = data_loading_tio(self.params['validation_files_csv'],
                                                                      self.params['training_patch_size'],
                                                                      self.params['max_queue_length'],
                                                                      self.params['samples_per_volume'],
                                                                      self.params['batch_size'],
                                                                      self.params['n_workers'],
                                                                      pin_mem,
                                                                      transforms_c,
                                                                      self.params['load_segmentation'])
            else:
                self.data['train_dataloader'] = cmf.get_dataloader(dataset_c_train, self.params['train_files_csv'],
                                                                   self.params['batch_size'],
                                                                   self.params['train_flag'],
                                                                   self.params['n_workers'])
                self.data['validation_dataloader'] = cmf.get_dataloader(dataset_c_train, self.params['validation_files_csv'],
                                                                        self.params['batch_size'],
                                                                        False,  # Is not mandatory for training
                                                                        self.params['n_workers'])

        if (not self.params['use_patch_inference']) and self.params['test_flag']:
            # Patch based dataloading is in .test()
            dataset_c_test = self.problem_handler.test_dataset_class  # Custom Dataset, defined in dataset_classes and chosen in .ini
            self.data['test_dataloader'] = cmf.get_dataloader(dataset_c_test,
                                                              self.params['test_files_csv'],
                                                              batch_size=1,
                                                              required=self.params['test_flag'])

    def train(self):
        """
        Train the model using the previously set up parameters.
        """
        if self.params['resume_training'] != "":  # Continue training a model
            self.models['main'] = torch.load(self.params['resume_training'], map_location=str(self.params['device']))
        else:
            if self.params['device'] == torch.device('cuda') and torch.cuda.device_count() > 1:
                print("Using ", torch.cuda.device_count(),
                      " GPUs!!. For disabling this, set the CUDA_VISIBLE_DEVICVES variable")
                self.models['main'] = eval(self.params['model_class'])()
                self.models['main'] = nn.DataParallel(self.models['main'])
            else:
                self.models['main'] = eval(self.params['model_class'])()  # Instance a model
            self.models['main'].to(self.params['device'])

        if self.params['acnn_lambda'] != 0:  # If ACNN regularization is needed, load the trained model
            self.models['acnn'] = eval(self.params['acnn_class'])() if self.params['acnn_lambda'] else None
            if self.params['acnn_path']:
                mod = torch.load(self.params['acnn_path'])  # TODO Load the models the same way
                self.models['acnn'].load_state_dict(mod.state_dict())
                self.models['acnn'].eval().to(self.params['device'])  # Set eval mode on (lock the weights)

        # Configure the optimizer
        if self.params['optimizer'] == "adam":
            self.params['optimizer'] = optim.Adam(self.models['main'].parameters(), lr=self.params['learning_rate'],
                                                  weight_decay=self.params['weight_decay'], amsgrad=True)
        elif self.params['optimizer'] == "rmsprop":
            self.params['optimizer'] = optim.RMSprop(self.models['main'].parameters(), lr=self.params['learning_rate'],
                                                     weight_decay=self.params['weight_decay'],
                                                     momentum=self.params['momentum'])
        elif self.params['optimizer'] == "sgd":
            self.params['optimizer'] = optim.SGD(self.models['main'].parameters(), lr=self.params['learning_rate'],
                                                 momentum=self.params['momentum'],
                                                 weight_decay=self.params['weight_decay'])

        cmf.print_params_dict(self.params)  # Print the params dictionary

        self.current_train_iteration = 0
        for n_epoch in range(1, self.params['n_epochs'] + 1):  # Training n_epochs
            ep_time = cmf.tic()  # Begin measuring time
            self.current_epoch = n_epoch

            print("Epoch: ", n_epoch)
            self.forward_pass('train', self.data['train_dataloader'])
            self.update_plots_tensorboard_avg('train', n_epoch)

            self.forward_pass('val', self.data['validation_dataloader'])
            self.update_plots_tensorboard_avg('val', n_epoch)

            # Calculate remaining time (for display)
            cmf.toc_eps(ep_time, n_epoch, self.params['n_epochs'])  # Stop measuring time and estimate remaining time.

            # Autosave model, configuration and plots
            # self.update_plots() if n_epoch % self.params['plot_update_interval'] == 0 else 0  # TODO REMOVE MATPLOTLIB

            if self.params['train_flag'] and (n_epoch % self.params['autosave_epochs']) == 0:
                cmf.save_model(self.models['main'], self.out_model_path, self.cfg_path, n_epoch,
                               save_checkpoint=True)  # Save epoch checkpoint
                if self.params['test_flag']:
                    self.test(torchio_pred=self.params['use_patch_inference'])  # Predict on test data
                # self.update_plots(epoch_number=n_epoch)  # Update main plots

            cmf.save_model(self.models['main'], self.out_model_path)  # Update the model on each epoch

    def test(self, torchio_pred=False):
        # If the model isn't loaded yet, load it.
        if self.models['main'] is None:
            if str(self.params['device']) == 'cuda':
                self.models['main'] = torch.load(self.out_model_path, map_location=str(self.params['device']))
            else:
                self.models['main'] = torch.load(self.out_model_path, map_location='cpu')

        if self.params['test_flag'] and self.params['test_files_csv'] == '':
            print("No csv provided for testing")
        elif not torchio_pred:  # Regular inference
            print("Images to test: ", os.path.split(self.params['test_files_csv'])[0])
            cmf.print_params_dict(self.params)  # Print the params dictionary
            self.forward_pass('test', self.data['test_dataloader'])
        else:  # Patch-based inference
            patch_inference_tio(self.params['test_files_csv'],
                                self.models['main'],
                                self.params['inference_patch_size'],
                                patch_overlap=self.params['patch_overlap'],
                                batch_size=self.params['batch_size'],
                                device=self.params['device'])

    def update_plots(self, epoch_number=None):  # TODO Try to remove from this class
        """ Update the loss and dice (optional) plots.

        :param epoch_number: If provided, it additionaly generates another image that won't be overwrited after with the
        curves up to that epoch
        """
        if epoch_number is not None:
            out_name = self.params['name'] + '_ep' + str(epoch_number)
        else:
            out_name = self.params['name']

        cmf.array_to_plot([self.losses_and_metrics['train/epoch/loss'], self.losses_and_metrics['val/epoch/loss']],
                          ['train loss', 'validation loss'],
                          name=out_name,
                          plotTitle=self.params['name'] + " model train and val losses",
                          x_label="Epoch", y_label="Loss")

        if self.params['save_dice_plots'] is True:
            cmf.array_to_plot([self.losses_and_metrics['train/epoch/dice'], self.losses_and_metrics['val/epoch/dice']],
                              ['dice train', 'dice validation'],
                              name=out_name,
                              plotTitle=self.params['name'] + " model train and dice coefficients",
                              x_label="Epoch", y_label="Dice")  # Todo try to remove form

    def forward_pass(self, phase, data_loader):
        """
        Make a forward pass on the network.

        :param phase: 'train', 'val', 'test'
        :param data_loader: The train/validation/test loader
        :return:
        """
        print("Phase: {}.".format(phase))

        # Clear the epoch losses
        # Todo clear the epoch losses (sum, add, replace with [])

        if phase == 'train':
            self.models['main'].train()
            torch.set_grad_enabled(True)  # Equivalent to 'with torch.no_grad()'
        else:  # eval mode (won't update parameters and disable dropout)
            self.models['main'].eval()  # TODO replace with method that excecutes eval in all the self.models
            torch.set_grad_enabled(False)

        out_folder = ''  # Will be assigned when saving an image
        for batch_idx, sample in enumerate(data_loader):  # for each batch in the split
            # Get the corresponding input and target, which may vary depending on the model
            # input_img, target = self.get_input_and_target(sample, phase)

            # input_img, target, padding, filepath = self.get_input_and_target(sample, phase)

            # input_img, target = sample['image']['data'].to('cuda'), \
            #                     cmf.one_hot_encoding(sample['segmentation']['data'][:, 0]).to('cuda')

            # input_img, target = sample['image'].to('cuda').unsqueeze(0), \
            #                     cmf.one_hot_encoding(sample['segmentation']).to('cuda')

            input_img = sample['image'].to(self.params['device'])
            target = sample['target'].to(self.params['device']) if 'target' in sample else None
            filepath = sample['filepath']

            if phase == 'train':
                input_img.requires_grad_()  # Not necessary?
                # target.requires_grad_()  # Not necessary?

            model_out = self.models['main'](input_img)  # Forward pass

            if target is not None:  # If I have a Ground Truth for computing the loss
                self.compute_losses_and_metrics(model_out, target, batch_idx, len(data_loader))

            if phase == 'train':
                self.pt_loss.backward()
                self.params['optimizer'].step()
                self.params['optimizer'].zero_grad()
            elif phase == 'test':  # After the forward pass I have to save the file
                self.problem_handler.write_prediction(model_out, filepath, self.params['name'], input_img)
                # self.write_predictions(input_img, filepath, target, model_out, padding)  # todo check out

        # self.loss_avgs(phase)  # Compute epoch loss as average

    # def write_predictions(self, input_img, filepath, target, model_out, padding):
    #     """ Get predictions of a batch of test imagees and save them as SimpleITK images in a subfolder named "pred_"
    #     plus the name set in the cfg file in [MODEL][name].
    #
    #     :param input_img: batch of input images
    #     :param filepath: paths of the batch
    #     :param target: GT of the batch
    #     :param model_out: prediction of the batch
    #     :param padding: padding added to the image for the neural network
    #     :return:
    #     """
    #     for i in range(model_out.shape[0]):
    #         path, name = os.path.split(filepath[i])
    #         print("  ", name)
    #
    #         s_img = sitk.ReadImage(filepath[i])
    #         origin = s_img.GetOrigin()
    #         direction = s_img.GetDirection()
    #         spacing = s_img.GetSpacing()
    #
    #         # Convert the input and output to a SimpleITK image and set the corresponding Origin and Direction
    #         np_input_img = cmf.unpad((input_img[i, 0, :, :, :].cpu().detach().numpy()), padding)  # Input image
    #         sitk_input_img = cmf.get_sitk_img(np_input_img, origin, direction, spacing)
    #
    #         # Convert One Hot encoding to Hard Segmentation (keep the highest probability)
    #         np_out = model_out[i, :, :, :, :].cpu().detach()  # Model prediction
    #         gt_out = target[i, :, :, :, :].cpu()
    #
    #         # Generate predictions folder and save the path string in outFolder
    #         out_folder = cmf.veri_folder(os.path.join(path, "pred_" + self.params['name']))
    #
    #         ext = '.' + name.split(os.extsep, 1)[1]
    #
    #         if self.params['model_type'] not in ['unetrec', 'flaprec', 'aerec', 'flaprecAE']:
    #             np_out = cmf.unpad(cmf.getHardSegmentationFromTensor(np_out).numpy(), padding)
    #             gt_out = cmf.unpad(cmf.getHardSegmentationFromTensor(gt_out).numpy(), padding)
    #         else:
    #             self.segPrefix = 'image_o'
    #
    #             np_out = cmf.unpad(cmf.getHardSegmentationFromTensor(np_out).numpy(), padding)
    #             gt_out = cmf.unpad(cmf.getHardSegmentationFromTensor(gt_out).numpy(), padding)
    #
    #             combined = cmf.keepMaxPixelValue(np_out, gt_out)
    #             c_gt = cmf.get_sitk_img(combined, origin, direction, spacing)
    #
    #             sitk.WriteImage(c_gt, os.path.join(out_folder, name.replace(ext, '_combined' + ext)))
    #
    #         sitk_gt = cmf.get_sitk_img(gt_out, origin, direction, spacing)
    #         sitk_out = cmf.get_sitk_img(np_out, origin, direction, spacing)
    #
    #         # Write images in outFolder
    #         if self.params['model_type'] in ['unetrec', 'flaprec', 'aerec', 'flaprecAE']:
    #             name_inp = name.replace(ext, 'nninput' + ext)
    #         else:
    #             name_inp = name
    #
    #         sitk.WriteImage(sitk_input_img, os.path.join(out_folder, name_inp))  # Input of the model
    #         sitk.WriteImage(sitk_out, os.path.join(out_folder, name.replace(ext,
    #                                                                         '_reconstr' + ext)))  # Model output
    #
    #         if self.params['model_type'] in ['unetrec', 'flaprec', 'aerec', 'flaprecAE']:
    #             sitk_diff = cmf.diff_sitk(sitk_input_img, sitk_out)  # Compute difference between images
    #             sitk_diff_eroded = cmf.erode_image_sitk(sitk_diff)
    #             # sitk_diff_cc = cmf.retainLargestConnectedComponent(sitk_diff)
    #
    #             sitk.WriteImage(sitk_diff,
    #                             os.path.join(out_folder, name.replace(ext, '_diff' + ext)))
    #             sitk.WriteImage(sitk_diff_eroded,
    #                             os.path.join(out_folder, name.replace(ext, '_diff_eroded' + ext)))
    #             # sitk.WriteImage(sitk_diff_cc,
    #             #                 os.path.join(out_folder, name.replace(ext, '_diff_cc' + ext)))
    #
    #         if self.params['model_type'] in ['unetrec', 'flaprec', 'aerec', 'flaprecAE']:
    #             gt_prefix = 'groundtruth'
    #         else:
    #             gt_prefix = self.segPrefix
    #
    #         sitk.WriteImage(sitk_gt, os.path.join(out_folder, name.replace(ext, gt_prefix + ext)))
    #
    #         return out_folder
    #     return None
    #
    # def get_input_and_target(self, sample, phase):
    #     """" Depending the model we're training, the input and target images may change.
    #
    #     :param sample: BrainSegmentationDataset sample
    #     :param phase: 'train' 'validation' or 'test'.
    #     :return:
    #     """
    #
    #     # padding, filepath = sample['padding'], sample['filepath']
    #     padding, filepath = ((0, 0), (0, 0), (0, 0)), sample['filepath']
    #     input_img = sample['image'].to(self.params['device'])  # In the general case the input is the image.
    #     # ground_truth = sample['segmentation']
    #
    #     # input_img = torch.squeeze(sample['image'][torchio.DATA], 1).to(self.params['device'])
    #     # ground_truth = torch.squeeze(sample['segmentation'][torchio.DATA], 1).to(self.params['device'])
    #
    #     # U-NET for segementation: input is a CT scan, GT the segmentation
    #     if self.params['model_type'] == 'unet':
    #         target = cmf.one_hot_encoding(sample['segmentation']).to(self.params['device'])  # Encode the target
    #
    #     # ACNN AE: Input is the noisy segmentation and the output is the same segmentation
    #     elif self.params['model_type'] == 'acnn':
    #         ground_truth = sample['image']
    #         if phase == 'train':  # Add salt and pepper noise
    #             input_img = torch.tensor(cmf.salt_and_pepper(sample['image'].detach().numpy()), dtype=torch.float32)
    #         else:
    #             input_img = torch.tensor(sample['image'].detach().numpy(), dtype=torch.float32)
    #         target = cmf.one_hot_encoding(ground_truth).to(self.params['device'])  # Encode the target
    #
    #     # U-NET and AE for skull reconstruction under (simulated) craniectomy.
    #     elif self.params['model_type'] in ['unetrec', 'aerec']:
    #         ground_truth = sample['image']  # The GT is the complete skull
    #         if phase != 'test':
    #             input_cropped, _ = cmf.random_blank_patch_wrapper(sample['image'], prob=.8, return_extracted=True)
    #             input_cropped_sp = cmf.salt_and_pepper(input_cropped, noise_probability=.8, noise_density=.5)
    #             input_img = torch.tensor(input_cropped_sp, dtype=torch.float32)
    #         else:
    #             ground_truth = ground_truth
    #         target = cmf.one_hot_encoding(ground_truth).to(self.params['device'])  # Encode the target
    #
    #     # U-NET and AE for flap estimation under (simulated) craniectomy.
    #     elif self.params['model_type'] in ['flaprec', 'flaprecAE']:
    #         if phase != 'test':  # during training add noise and simulate bone flap extraction
    #             input_cropped, flap = cmf.random_blank_patch_wrapper(sample['image'], prob=0.8, return_extracted=True)
    #             input_cropped_sp = cmf.salt_and_pepper(input_cropped, noise_probability=.01, noise_density=.05)
    #             flap_pt = torch.tensor(flap, dtype=torch.float32)
    #             input_img = torch.tensor(input_cropped_sp, dtype=torch.float32)
    #             # input_img = torch.tensor(input_cropped_sp, dtype=torch.float32)
    #
    #             target = cmf.one_hot_encoding(flap_pt).to(self.params['device'])  # Encode the target
    #         else:
    #             target = cmf.one_hot_encoding(sample['image']).to(self.params['device'])  # Encode the target
    #
    #     input_img = input_img.unsqueeze(1).to(self.params['device'])  # Add the channel dimension
    #
    #     # return input_img, target  # todo return depending phase
    #     return input_img, target, padding, filepath

    def compute_losses_and_metrics(self, model_out, target, idx, N):
        """ Calculate the losses depending on the model phase, and save it in self.pt_loss
        :param model_out:
        :param target:
        :param idx:
        :param N:
        :return:
        """
        self.pt_loss = []

        # /\ Loss functions /\
        if self.params['ce_lambda'] != 0:  # Binary cross entropy between model output and target
            if 'ce' not in self.losses_and_metrics:
                self.losses_and_metrics['ce'] = []
            self.pt_loss.append(self.params['ce_lambda'] * nn.BCELoss()(model_out, target))
            self.losses_and_metrics['ce'].append(float(self.pt_loss[-1]))

        if self.params['acnn_lambda'] != 0:
            if self.models['acnn'] is not None:  # MSE between the codes (if ACNN loaded)
                acnn_model_out_code, shape1 = self.models['acnn'].encode(model_out[:, 1:])  # Segm. encoding
                acnn_gt_code, shape2 = self.models['acnn'].encode(target[:, 1:])  # GT encoding
                self.pt_loss.append(
                    self.params['acnn_lambda'] * F.mse_loss(acnn_model_out_code, acnn_gt_code))  # MSE of the codes
                self.losses_and_metrics['mse'].append(float(self.pt_loss[-1]))
            else:
                raise Exception("acnn_lambda != 0 but the model is not loaded! Check if acnn_path is correct.")

        if self.params['dice_lambda'] != 0:  # Dice loss
            if 'dice_loss' not in self.losses_and_metrics:  # todo add to all
                self.losses_and_metrics['dice_loss'] = []
            self.pt_loss.append(self.params['dice_lambda'] * cmf.dice_loss()(model_out, target))
            self.losses_and_metrics['dice_loss'].append(float(self.pt_loss[-1]))

        # /\ Metrics /\
        if self.params['save_dice_plots'] is True:  # Dice coefficient
            if 'dice_coef' not in self.losses_and_metrics:
                self.losses_and_metrics['dice_coef'] = []
            dice_coeff = cmf.dice_coeff(model_out, target)
            self.losses_and_metrics['dice_coef'].append(dice_coeff)

        self.pt_loss = sum(self.pt_loss)  # I'll convert the array into a PyTorch tensor

        if 'epoch_loss' not in self.losses_and_metrics:  # todo add to all
            self.losses_and_metrics['epoch_loss'] = []
        self.losses_and_metrics['epoch_loss'].append(float(self.pt_loss))

        print('    Batch {}/{} ({:.0f}%)\tLoss: {:.6f}'.format(idx + 1, N, 100. * (idx + 1) / N, float(self.pt_loss)))

    #
    # def get_loss(self, phase, model_out, target, batch_idx, n_imgs):
    #     """ Calculate the losses depending on the model phase.
    #
    #     :return:
    #     """
    #     loss = torch.tensor(0.).to(self.params['device']).requires_grad_()
    #     if phase in ['train', 'validation', 'val']:
    #         if self.params['model_type'] in ['unet', 'unetrec', 'flaprec', 'aerec', 'flaprecAE']:
    #             if self.params['ce_lambda'] > 0:  # Binary cross entropy between model output and target
    #                 BCE = nn.BCELoss()(model_out, target)
    #                 self.loss_arrays['ce'].append(float(BCE))
    #                 loss = loss + self.params['ce_lambda'] * BCE
    #
    #             if hasattr(self, 'acnnModel') and self.models[
    #                 'acnn'] is not None:  # MSE between the codes (if ACNN loaded)
    #
    #                 acnn_model_out_code, shape1 = self.models['acnn'].encode(
    #                     model_out[:, 1:, :, :, :])  # Segm. encoding
    #                 acnn_gt_code, shape2 = self.models['acnn'].encode(target[:, 1:, :, :, :])  # GT encoding
    #
    #                 mse = self.params['acnn_lambda'] * F.mse_loss(acnn_model_out_code,
    #                                                               acnn_gt_code)  # MSE Loss of the codes
    #                 loss = loss + mse  # Sum to other losses
    #                 self.loss_arrays['mse'].append(float(mse))
    #
    #             if self.params['save_dice_plots'] is True:  # Dice coefficient
    #                 dice_coeff = cmf.dice_coeff(model_out, target)
    #                 self.metric_arrays['dice'].append(dice_coeff)
    #                 if phase == 'train':
    #                     self.writer.add_scalar(phase + '/dice (per iteration)', dice_coeff, self.current_train_iteration)
    #
    #             if self.params['dice_lambda'] != 0:  # Dice loss
    #                 dice = self.params['dice_lambda'] * cmf.dice_loss()(model_out, target)
    #                 self.loss_arrays['dice'].append(float(dice))
    #                 loss = loss + dice
    #
    #             self.loss_arrays['epoch'].append(float(loss))  # Save and print the loss
    #             perc = 100. * (batch_idx + 1) / n_imgs
    #             if phase != 'test':
    #                 print('    Batch {}/{} ({:.0f}%)\tLoss: {:.6f}'.format(batch_idx + 1, n_imgs, perc, loss))
    #
    #         elif self.params['model_type'] == 'acnn':  # Autoencoder (ACNN) training
    #             loss = F.binary_cross_entropy(model_out, target)
    #             self.loss_arrays['epoch'].append(float(loss))
    #             perc = 100. * (batch_idx + 1) / n_imgs
    #             print('    Batch {}/{} ({:.0f}%)\tLoss: {:.6f}'.format(batch_idx + 1, n_imgs, perc, loss))
    #
    #         if phase == 'train':
    #             self.writer.add_scalar(phase + '/loss (per iteration)', float(loss), self.current_train_iteration)
    #     return loss
    #
    # def loss_avgs(self, phase):
    #     """ Save the loss average, and print the losses.
    #
    #     :param phase:
    #     :return:
    #     """
    #     if phase in ['train', 'validation', 'val']:  # Only on training
    #         avg_loss = np.mean(self.loss_arrays['epoch'])  # Average of all batches
    #         avgLossMsg = '\t Avg loss: {:.4f}'.format(avg_loss)
    #
    #         avg_dice_coef = []
    #         if self.params['save_dice_plots']:
    #             avg_dice_coef = np.mean(self.metric_arrays['dice'])
    #             self.writer.add_scalar(phase + '/dice (per epoch)', avg_dice_coef, self.current_epoch)
    #
    #         if phase == 'train':
    #             self.losses_and_metrics['train/epoch/loss'].append(avg_loss)
    #             if self.params['save_dice_plots']:
    #                 self.losses_and_metrics['train/epoch/dice'].append(avg_dice_coef)
    #         elif phase == 'val':
    #             self.losses_and_metrics['val/epoch/loss'].append(avg_loss)
    #             if self.params['save_dice_plots']:
    #                 self.losses_and_metrics['val/epoch/dice'].append(avg_dice_coef)
    #         elif phase == 'test':
    #             self.losses_and_metrics['test/epoch/loss'].append(avg_loss)
    #
    #         if self.params['model_type'] in ['unet', 'unetrec', 'flaprec', 'aerec', 'flaprecAE']:
    #             if self.params['ce_lambda'] > 0:
    #                 avg_bce = np.mean(self.loss_arrays['ce'])  # Mean BCE for the epoch
    #                 avgLossMsg += ', Avg BCE.: {:.4f} (x{})'.format(avg_bce, self.params['ce_lambda'])
    #             if self.models['acnn'] is not None:  # Mean MSE for the epoch
    #                 avg_mse = np.mean(self.loss_arrays['mse'])
    #                 avgLossMsg += ', Avg MSE.: {:.4f} (x{})'.format(avg_mse, self.params['acnn_lambda'])
    #             if self.params['dice_lambda'] != 0:  # Mean Dice for the epoch
    #                 avg_dice = np.mean(self.loss_arrays['dice'])
    #                 avgLossMsg += ', Avg Dice.: {:.4f} (x{})'.format(avg_dice, self.params['dice_lambda'])
    #                 self.writer.add_scalar(phase + '/loss (per epoch)', avg_dice, self.current_epoch)
    #
    #         print(avgLossMsg)

    def update_plots_tensorboard_avg(self, phase, i, type='epoch', print_to_console=True):
        for key in self.losses_and_metrics.keys():
            avg = sum(self.losses_and_metrics[key]) / len(self.losses_and_metrics[key])
            self.writer.add_scalar(phase + '/' + type + '/' + key, float(avg), i)
            self.losses_and_metrics[key] = []
            if print_to_console:
                print('{} {} average: {}.'.format(type, i, float(avg)))


# def predict_mask_here(model, img_sitk=None, img_path=None, device='cpu'):
#     """ Predicts the mask of a single file and returns it
#
#     :param model: PyTorch trained model path.
#     :param img_sitk: SimpleITK image in which is made the mask prediction. Can be omitted and use a path with img_path.
#     :param img_path: If img_ocv is not provided, this parameter specifies the path of an image to make the prediction.
#     :param device: Device to use (gpu or cpu).
#     """
#     print("   Predicting brain mask...", end=" ")
#
#     if device == 'cuda' or device == 'gpu':
#         # net = torch.load(model, map_location=lambda storage, loc: storage.cuda(0))  # Load trained model
#         net = torch.load(model)  # Load trained model
#     else:
#         net = torch.load(model, map_location='cpu')  # Load trained model
#     if img_sitk is None:
#         if img_path is None:
#             print("Image not provided!")
#             return None
#         img_sitk = sitk.ReadImage(img_path)  # Load image
#
#     o_size = img_sitk.GetSize()
#     o_spacing = img_sitk.GetSpacing()
#     o_direction = img_sitk.GetDirection()
#     o_origin = img_sitk.GetOrigin()
#
#     img_sitk = sitk.GetArrayFromImage(img_sitk)
#     img_pt = torch.tensor(cmf.padVolumeToMakeItMultipleOf(img_sitk, [16, 16, 16]), dtype=torch.float32).to(device)
#     img_pt = img_pt.unsqueeze(0)  # Add the channel dimension after the file id of the batch.
#     img_pt = img_pt.unsqueeze(0)  # Add the channel dimension after the file id of the batch.
#
#     model_out = net(img_pt)[0, :, :, :, :]
#
#     pt_out = cmf.getHardSegmentationFromTensor(model_out).cpu().detach()
#
#     o_image = sitk.GetImageFromArray(pt_out.numpy())  # output image
#     o_image = o_image[0:o_size[0], 0:o_size[1], 0:o_size[2]]  # Remove padding
#     o_image.SetSpacing(o_spacing)
#     o_image.SetDirection(o_direction)
#     o_image.SetOrigin(o_origin)
#     o_image = cmf.retainLargestConnectedComponent(o_image)  # Keep the biggest connected component
#     o_image = sitk.Cast(o_image, sitk.sitkFloat32)
#
#     print("done.")
#     return o_image
#

def ttm_cmd():
    parser = argparse.ArgumentParser(description=textwrap.dedent('''\
         Train or test a model 
         --------------------------------
             Train or test a model for one of the following cases
                - unet.cfg: U-Net for image segmentation (-u argument).
                - acnn.cfg ACNN for learning an AE regularizer for helping out segmentation training (-n argument).
                
                - skullrec.cfg: U-Net for full skull reconstruction (-s argument).
                - skullrec_ae.cfg: AE for skull reconstruction (-a argument).
                - flaprec.cfg: U-Net for flap only reconstruction (-f argument). 
                
                - Custom cfg: Insert a cfg file path (-c argument).
                
               FIRST CONFIGURE THE CORRESPONDING FILE, and then run this command with the corresponding argument.
               The configuration can be performed in a text editor, and sample configurations can be found in the repo.
               You can't set more than one of these arguments at the same time.
         '''), formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-u', '--unet', action='store_true', help="unet.cfg")
    parser.add_argument('-n', '--acnn', action='store_true', help="acnn.cfg")
    parser.add_argument('-s', '--skullrec', action='store_true', help="skullrec.cfg")
    parser.add_argument('-a', '--skullrec_ae', action='store_true', help="skullrec_ae.cfg")
    parser.add_argument('-f', '--flaprec', action='store_true', help="flaprec.cfg")
    parser.add_argument('-c', '--custom', help="Custom cfg")

    args = parser.parse_args()

    if args.custom:
        ModelLoader(args.custom)
        return

    if np.count_nonzero(list(vars(args).values())) > 2:
        print("You have chosen more than one argument. Select only one of them.")
        return
    elif np.count_nonzero(list(vars(args).values())) == 0:
        print("You have to set an argument to continue. See help for more info.")
        return

    if args.unet:
        ModelLoader('cfg/unet.cfg')
    if args.acnn:
        ModelLoader('cfg/acnn.cfg')
    if args.skullrec:
        ModelLoader('cfg/skullrec.cfg')
    if args.skullrec_ae:
        ModelLoader('cfg/skullrec_ae.cfg')
    if args.flaprec:
        ModelLoader('cfg/flaprec.cfg')


if __name__ == '__main__':
    # ttm_cmd()
    ModelLoader("cfg/aimpl.ini")
