import os
from abc import ABC, abstractmethod

import torch
import common as utils
from dataset_classes import *
import SimpleITK as sitk
import common as cmf

class ProblemHandler:
    """ Abstract class for defining the """

    def __init__(self, train_dataset_class, test_dataset_class):
        self.train_dataset_class = None if train_dataset_class is None else train_dataset_class
        self.test_dataset_class = None if test_dataset_class is None else test_dataset_class

    @abstractmethod
    def write_prediction(self, predictions, input_filepaths, output_folder_name): pass  # Method used for saving pred.


class ImageTargetProblem(ProblemHandler, ABC):
    """ The FlapRecHandler will simulate flaps for training but not for testing."""
    def __init__(self, TrainDataset, TestDataset):
        super(ImageTargetProblem, self).__init__(TrainDataset, TestDataset)

    def write_prediction(self, predictions, input_filepaths, output_folder_name, input_imgs):
        """ Get predictions of a batch of test images and save them as SimpleITK images.

        :param input_filepaths: Paths of the input images in the batch.
        :param predictions: prediction of the batch.
        :param output_folder_name: Name of the folder with the outputs.
        :return:
        """
        print(" Saving prediction for...")
        for prediction, input_filepath, input_img in zip(predictions, input_filepaths, input_imgs):  # For each element in the batch
                path, name = os.path.split(input_filepath)
                print("  " + name + "..", end='')

                # Load the original image again and replace the image array with the prediction
                sitk_orig_img = sitk.ReadImage(input_filepath)
                origin, direction, spacing = cmf.get_sitk_metadata(sitk_orig_img)

                np_out = prediction.cpu().detach()  # Model prediction
                np_out = cmf.hard_segm_from_tensor(np_out).numpy()

                sitk_out = cmf.get_sitk_img(np_out, origin, direction, spacing)

                out_folder = cmf.veri_folder(os.path.join(path, "pred_" + output_folder_name))
                sitk.WriteImage(sitk_out, os.path.join(out_folder, name))
                sitk.WriteImage(cmf.get_sitk_img(input_img[0].cpu().detach(), origin, direction, spacing), os.path.join(out_folder, name.replace('.nii.gz', '_i.nii.gz')))

                print("saved.")


class FlapRecHandler(ImageTargetProblem):
    """ The FlapRecHandler will simulate flaps for training but not for testing."""
    def __init__(self):
        super(FlapRecHandler, self).__init__(FlapRecTrainDataset, NiftiImageOnly)


class BinaryImageMask(ImageTargetProblem):
    """ For training it will load the image and mask, and for test only the image."""
    def __init__(self):
        super(BinaryImageMask, self).__init__(BrainSegmentationDataset, NiftiImageOnly)


class Mixed_BinaryImageMask_FlapRec(ImageTargetProblem):
    """ For training it will load the image and mask, and for test only the image."""
    def __init__(self):
        super(Mixed_BinaryImageMask_FlapRec, self).__init__(SelectiveFRDataset, NiftiImageOnlyWithAtlas)
