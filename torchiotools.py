import os

import pandas
import torch
# import torchio

import common as cmf
import SimpleITK as sitk
import numpy as np


def get_subjects_flaprec(split_files_csv, load_segmentation=False):
    """ Read a csv file and return a subjects list containing torchio Subjects

    :param split_files_csv: csv file path.
    :param load_segmentation: Load also the segmentation image.
    :return:
    """
    files_split = pandas.read_csv(split_files_csv)

    subjects = []
    for i, files in enumerate(files_split.values):  # todo adjust for making it suitable for other types of problems
        image_path = files[0]
        segm_path = files[1] if load_segmentation else None
        print("[{}/{}] Imagen: ".format(i, len(files_split.values)), image_path)

        tio_img = torchio.Image(image_path, type=torchio.LABEL, channels_last=False)
        tio_seg = torchio.Image(segm_path, type=torchio.LABEL, channels_last=False) if segm_path else tio_img

        image_dict = {
            'image': tio_img,
            'segmentation': tio_seg,
            'filepath': str(tio_img.path)
        }
        subject = torchio.Subject(image_dict)
        subjects.append(subject)
    return subjects


def data_loading_tio(split_files_csv, patch_size=160, max_queue_length=3, samples_per_volume=1, batch_size=2,
                     n_workers=1, pin_memory=True, transforms=None, load_segmentation=False):
    subjects = get_subjects_flaprec(split_files_csv, load_segmentation=load_segmentation)

    dataset = torchio.SubjectsDataset(subjects, transform=transforms)

    sampler = torchio.sampler.UniformSampler(patch_size)
    # sampler = torchio.sampler.LabelSampler(patch_size, 'segmentation', label_probabilities={0: 1, 1: 2})

    patches_tr_set = torchio.Queue(
        subjects_dataset=dataset,
        max_length=max_queue_length,
        samples_per_volume=samples_per_volume,
        sampler=sampler,
        shuffle_subjects=True,
        shuffle_patches=True,
        verbose=False,
        num_workers=n_workers
    )

    data_loader = torch.utils.data.DataLoader(patches_tr_set, batch_size=batch_size, pin_memory=pin_memory)

    return data_loader


def patch_inference_tio(csv_path, model, patch_size, out_path=None, patch_overlap=2, batch_size=5, device='cuda'):
    print("Performing patch-based inference")
    print("   Files: {}".format(csv_path))
    subjects = get_subjects_flaprec(csv_path)

    for subject in subjects:
        print("      subject: {}".format(subject['filepath']))
        grid_sampler = torchio.inference.GridSampler(subject, patch_size)
        # grid_sampler = torchio.inference.GridSampler(subject, patch_size, patch_overlap, padding_mode='mirror')
        aggregator = torchio.inference.GridAggregator(grid_sampler)
        patch_loader = torch.utils.data.DataLoader(grid_sampler, batch_size=batch_size)

        model.eval()
        with torch.no_grad():
            for n, patches_batch in enumerate(patch_loader):
                print("         Patches Batch {}/{}".format(n, len(patch_loader)))
                input_tensor = patches_batch['image']['data'].to(device)
                locations = patches_batch[torchio.LOCATION]
                logits = model(input_tensor)
                labels = logits.argmax(dim=1, keepdim=True)
                outputs = labels
                aggregator.add_batch(outputs, locations)
        print("         saving aggregated image...", end='')

        # tio_img2 = torchio.Image(subject['filepath'], type=torchio.LABEL)  # Original image with corresponding metadata
        # tio_img2[torchio.DATA] = aggregator.get_output_tensor()  # Replace the original image tensor with the prediction
        # Get hard segmentation and cast to int (unsqueeze due to torchio shape requirements)
        # sitk_im = sitk.Cast(tio_img2.as_sitk(), sitk.sitkUInt8)

        output = aggregator.get_output_tensor()
        sitk_im = sitk.GetImageFromArray(torch.squeeze(output, dim=0).to(torch.int8).numpy())

        if not out_path:  # output folder name
            out_path = 'pred'
        path, filename = os.path.split(subject['filepath'])

        output_path = os.path.join(cmf.veri_folder(os.path.join(path, out_path)), filename.replace('.nrrd', '.nii.gz'))
        sitk.WriteImage(sitk_im, output_path.replace('.nii.gz', '_pred.nii.gz'))

        # tio_img2[torchio.DATA] = subject['image']['data']
        # sitk_im = sitk.Cast(tio_img2.as_sitk(), sitk.sitkUInt8)
        # sitk.WriteImage(sitk_im, output_path.replace('.nii.gz', '_img.nii.gz'))

        # tio_img2[torchio.DATA] = subject['segmentation']['data']
        # sitk_im = sitk.Cast(tio_img2.as_sitk(), sitk.sitkUInt8)
        # sitk.WriteImage(sitk_im, output_path.replace('.nii.gz', '_flap.nii.gz'))
        print(" saved in {}/{}.".format(out_path, filename))
