import argparse
import math
import os
import textwrap

import SimpleITK as sitk
import numpy as np

import common

atlasPath = 'assets/atlas/atlas3_nonrigid_masked_1mm.nii.gz'
paramPath = 'assets/atlas/Par0000affine.txt'


class Preprocessor:
    def __init__(self, sitk_img=None, is_mask=False, save_path=""):
        if sitk_img is None:
            self.image = None
        else:
            self.image = sitk_img
        self.f_name = ""
        self.is_mask = is_mask

        # Transformation details of a registration (useful for later transformation of the masks)
        self.parameter_map = None

        self.direction = None
        self.origin = None

        # output folder
        self.save_path = save_path

    def open(self, file_name):
        """ Loads an image for pre processing.

        :param file_name: path of the file
        :return:
        """
        self.image = sitk.ReadImage(file_name)
        self.f_name = file_name

        self.direction = self.image.GetDirection
        self.origin = self.image.GetOrigin

        # Depending on the filename, the file may be a mask
        self.is_mask = self.f_name.endswith("head_mask.nii.gz")

    def set_filename(self, file_name=None):
        """ Change the default filename

        :param file_name: new file name.
        """
        if file_name:
            self.f_name = file_name
            return file_name
        else:
            return self.f_name

    def set_save_path(self, savePath):
        """ Set the path used for saving the preprocessed image in self.save_file()

        :param savePath: output folder.
        :return:
        """
        self.save_path = savePath
        common.veri_folder(self.save_path)

    def save_file(self, f_name=None):
        """ Save the preprocessed file in the folder set in set_save_path. The output filename will be the same as the
            input, but you can add some text to it with the f_name parameter.

        :param f_name: text to add to the original filename (optional).
        :return:
        """
        if not f_name:
            f_name = ""

        path_img, filename_img = os.path.split(self.f_name)  # Extract path and filename

        saved_img = os.path.join(self.save_path, f_name + filename_img)
        sitk.WriteImage(self.image, saved_img)  # Write image

        return saved_img

    def resample_spacing(self, target_spacing=None, direction=None, origin=None):
        """ Resample the image size (without deforming the image) and spacing for matching the spacing given as parameter.

        :param target_spacing: desired spacing.
        :param direction: custom direction.
        :param origin: custom origin.
        :return:
        """
        if target_spacing is None:
            return None
        if direction is None:
            direction = self.image.GetDirection()
        if origin is None:
            origin = self.image.GetOrigin()

        original_size = self.image.GetSize()
        original_spacing = self.image.GetSpacing()

        target_size = [int(math.ceil(original_size[0] * (original_spacing[0] / target_spacing[0]))),
                       int(math.ceil(original_size[1] * (original_spacing[1] / target_spacing[1]))),
                       int(math.ceil(original_size[2] * (original_spacing[2] / target_spacing[2])))]

        if self.is_mask:
            interpolator = sitk.sitkNearestNeighbor  # Binary images
        else:
            interpolator = sitk.sitkNearestNeighbor  # Binary images

            # interpolator = sitk.sitkLinear

        resampled_img = sitk.Resample(self.image, target_size, sitk.Transform(), interpolator, origin, target_spacing,
                                      direction, 0.0, self.image.GetPixelIDValue())

        self.image = resampled_img
        return resampled_img

    def apply_transform_selastix(self, parameter_map_path=None, img=None):
        """ Given a SimpleElastix parametermap, apply the transform to the image.

        :param parameter_map_path: Path of the parameter map with the transform matrix and other settings related.
        :param img: For using this method with other images than self.
        :return:
        """
        if img is not None:
            self.image = img

        parameter_map = sitk.ReadParameterFile(parameter_map_path)
        self.image = sitk.Transformix(self.image, parameter_map)  # self.image is the previously registered image

        return self.image

    def zero_threshold(self):
        pixel_id = self.image.GetPixelIDValue()
        self.image = self.image > 0
        sitk.Cast(self.image, pixel_id)
        return self.image

    def register_selastix(self, par_map, fixed_image_path=None, save_transform=False, binary=False):
        """ From a fixed image, and parametermap, register the self.image to the fixed image with SimpleElastix using
            such parameter map.

            You can find parameter maps in the Elastix parameter file database:
            http://elastix.bigr.nl/wiki/index.php/Parameter_file_database

        :param fixed_image_path: Fixed image used in registration.
        :param par_map: SimpleElastix parameter map.
        :param save_transform: Save the applied transform in a file with the same filename as the moving image plus
                "_reg.txt", wich can be used for transforming other images with Transformix, or calculating the inverse.
        :param binary: Image is binary.
        :return:
        """

        # Load the ParameterMap
        par_map = sitk.ReadParameterFile(par_map) if os.path.exists(par_map) else sitk.GetDefaultParameterMap(par_map)

        if self.is_mask:  # If it's a mask I need to use the previously saved ParMap and Nearest Neighbor interp.
            self.parameter_map = par_map

            self.parameter_map["ResampleInterpolator"] = ["FinalNearestNeighborInterpolator"]  # Switch to Nearest Neighbor
            self.parameter_map["DefaultPixelValue"] = ['0']  # New pixels will be zeros.

            result_image = sitk.Transformix(self.image, self.parameter_map)  # Apply transformation to mask
            self.image = result_image
        else:  # CT image
            fixed_image = sitk.ReadImage(fixed_image_path)

            reg_filter = sitk.ElastixImageFilter()  # Normal image, use the atlas and the default parameter map.

            reg_filter.LogToConsoleOff()  # Show elastix log or/and save to a file
            reg_filter.LogToFileOff()

            # Required for the latter part
            # original_size = self.image.GetSize() # Some parameters useful for the inverse
            # original_spacing = self.image.GetSpacing()
            # original_origin = self.image.GetOrigin()
            # original_direction = self.image.GetDirection()

            # Register the image
            reg_filter.SetFixedImage(fixed_image)
            reg_filter.SetMovingImage(self.image)

            extr_values = sitk.MinimumMaximumImageFilter()
            extr_values.Execute(self.image)
            min_val = extr_values.GetMinimum()
            max_val = extr_values.GetMaximum()

            par_map['DefaultPixelValue'] = [str(min_val)]  # Set background color for new pixels
            if binary:
                par_map["ResampleInterpolator"] = ["FinalNearestNeighborInterpolator"]  # Switch to NN

            reg_filter.SetParameterMap(par_map)
            reg_filter.Execute()
            result_image = reg_filter.GetResultImage()

            transform_parameter_map = reg_filter.GetTransformParameterMap(0)  # Save the registered image and param_map
            self.image = result_image
            self.clip_intensities(min_val, max_val)

            self.parameter_map = transform_parameter_map

            if save_transform is True:  # save the transform parametermap
                reg_param_file = os.path.join(self.save_path, os.path.split(self.f_name)[1] + "_reg.txt")
                reg_filter.WriteParameterFile(transform_parameter_map, reg_param_file)

                return reg_param_file

            # # --------------------------------
            # # Inverse parametermap calculation. This gives a SIGSEV error. Check updates in SimpleElastix and see if it's solved.
            # # https://github.com/SuperElastix/SimpleElastix/issues/167
            # # Although I do it manually, the SIGSEV error appears.
            #
            # invreg_filter = sitk.ElastixImageFilter()
            #
            # # For the inverse transform calculation we need to set the original fixed image as both fixed and moving image
            # # (section 6.1.6 of the elastix manual)
            # original_fixed_image = sitk.ReadImage(fixed_image_path)
            # invreg_filter.SetFixedImage(original_fixed_image)
            # invreg_filter.SetMovingImage(original_fixed_image)
            #
            # # Take the parameter map of the previous registration filter_name
            # inv_par_map = sitk.ReadParameterFile(paramPath)
            #
            # # This metric_str makes elastix find the inverse.
            # inv_par_map['Metric'] = ['DisplacementMagnitudePenalty']
            #
            # invreg_filter.SetParameterMap(inv_par_map)
            # invreg_filter.SetInitialTransformParameterFileName(reg_param_file)  # Provide the previous registration as initialization
            #
            # invreg_filter.Execute()  # Get the inverse transform
            #
            # inv_par_map = invreg_filter.GetTransformParameterMap(0)
            # # inverse_reg_matrix = list(inv_par_map['TransformParameters'])  # This is the inverse matrix, but with a wrong displacement
            # # reg_matrix = transform_parameter_map['TransformParameters']  # Registration matrix
            # # inverse_reg_matrix[9:] = list(-1 * np.array(reg_matrix[9:], dtype=np.float))  # Change the displecement to minus the registration displacement
            # # inv_par_map['TransformParameters'] = [str(f) for f in list(inverse_reg_matrix)]  # Write the inverse matrix to the parameter map.
            #
            # # Manually set the basic image parameters
            # inv_par_map['Size'] = [str(f) for f in list(original_size)]
            # inv_par_map['Spacing'] = [str(f) for f in list(original_spacing)]
            # inv_par_map['Origin'] = [str(f) for f in list(original_origin)]
            # inv_par_map['Direction'] = [str(f) for f in list(original_direction)]
            # inv_par_map['Direction'] = [str(f) for f in list(original_direction)]
            # inv_par_map['DefaultPixelValue'] = [str(-1000)]
            #
            # inv_par_map["InitialTransformParametersFileName"] = ["NoInitialTransform"]  # As mentioned in the elastix manual
            #
            # # Save the inverse transform
            # reg_param_file = os.path.join(self.save_path, os.path.split(self.f_name)[1] + "_invreg.txt")
            # reg_filter.WriteParameterFile(inv_par_map, reg_param_file)

    def z_scores_normalization(self):
        """ Normalize intenisities using z-scores

        :return:
        """
        data = sitk.GetArrayFromImage(self.image)

        # data has integer values (data.dtype), so it needs to be casted to float
        data_aux = data.astype(float)

        # Find mean, std then min_max_normalization
        mean = data_aux.mean()
        std = data_aux.std()
        data_aux = data_aux - float(mean)
        data_aux = data_aux / float(std)

        # Save edited data
        out_img = sitk.GetImageFromArray(data_aux)
        out_img.SetSpacing(self.image.GetSpacing())
        out_img.SetOrigin(self.image.GetOrigin())
        out_img.SetDirection(self.image.GetDirection())

        self.image = out_img

    def resample_to_maxpx(self, maxpx=None):
        """ Resize the image proportionately using maxpx as the max side size.

        :param maxpx: Largest side size. The other sides will scale proportionately.
        """
        if maxpx is None:
            return None

        original_size = self.image.GetSize()
        maxval = max(original_size)

        target_spacing = list(self.image.GetSpacing())
        target_spacing = [1.0 * x * maxval / maxpx for x in target_spacing]

        if self.is_mask:
            interpolator = sitk.sitkNearestNeighbor
        else:
            interpolator = sitk.sitkLinear

        target_size = [maxpx, maxpx, maxpx]
        resampled_img = sitk.Resample(self.image, target_size, sitk.Transform(),
                                      interpolator, self.image.GetOrigin(),
                                      target_spacing, self.image.GetDirection(), 0.0,
                                      self.image.GetPixelIDValue())

        self.image = resampled_img

    def clip_intensities(self, imin, imax):
        """ Clip the intensity values of self.image, making values outside that range imin or imax.

        :param imin: Minimum value admitted
        :param imax: Maximum value admitted
        """
        arr = sitk.GetArrayFromImage(self.image)
        outimg = sitk.GetImageFromArray(np.clip(arr, imin, imax))
        outimg.SetSpacing(self.image.GetSpacing())
        outimg.SetOrigin(self.image.GetOrigin())
        outimg.SetDirection(self.image.GetDirection())
        self.image = outimg

    def min_max_normalization(self):
        """ Map the image intensities to the [0, 1] range.
        """
        self.image = sitk.Cast(self.image, sitk.sitkFloat32)

        arr = sitk.GetArrayFromImage(self.image)  # For getting the max and min values
        outimg = ((self.image - arr.min()) / (arr.max() - arr.min()))

        self.image = outimg

    def convert_to_fixed_size(self, fixed_size_pad):
        arr = sitk.GetArrayFromImage(self.image)
        outimg = sitk.GetImageFromArray(common.fixed_pad(arr, fixed_size_pad))
        outimg.SetSpacing(self.image.GetSpacing())
        outimg.SetOrigin(self.image.GetOrigin())
        outimg.SetDirection(self.image.GetDirection())
        self.image = outimg

    def keep_largest_cc(self):
        self.image = common.retainLargestConnectedComponent(self.image)

    def crop_random_blank_patch(self):
        self.image = common.random_blank_patch_wrapper(self.image, is_tensor=False)


def prep_image_or_mask(image_path=None, output_folder=None, mask_path=None, clip_intensity_values=None,
                       target_spacing=None, fixed_size_pad=None, threshold=False, largest_cc=False, register=False,
                       random_blank_patch=False, binary=False, parameter_map=None, atlas_path=None):
    """ From an image, mask (both optional) and output path, it preprocess the images to the output folder (if provided)
        or it returns the preprocessed SimpleITK image.

    :param random_blank_patch:
    :param parameter_map: Path of the parametermap if register=True.
    :param atlas_path: Path of the atlas if register=True.
    :param image_path: input image (optional)
    :param output_folder: output folder, where the preprocessed images will be saved.
    :param mask_path: input mask (optional)
    :param clip_intensity_values: List with two intensity values for clipping itensities in that range (optional).
    :param target_spacing: List contanining a custom spacing for resampling the image for fitting it (optional).
    :param fixed_size_pad: List with a fixed size for zero-padding the image for reaching that size (optional).
    :param fixed_size_pad: List with a fixed size for zero-padding the image for reaching that size (optional).
    :param threshold: Apply threshold to non binary images.
    :param largest_cc: keep the largest connected component.
    :param register: Apply rigid registration to images.
    :param binary: Image is binary (Nearest Neighbor interpolation needed).

    :return:
    """

    if os.path.exists(image_path):
        print(("  Image: {}".format(image_path)))
        print("   Preprocessing...", end=" ")
        pp_image = Preprocessor(save_path=output_folder)
        pp_image.open(image_path)  # Load the file

        if output_folder:
            pp_image.set_save_path(output_folder)  # Create output folder if not exists

        if register:
            parameter_map = paramPath if not parameter_map else parameter_map
            atlas_path = atlasPath if not atlas_path else atlas_path
            reg_param_file = pp_image.register_selastix(parameter_map, atlas_path, save_transform=True, binary=binary)
        if clip_intensity_values is not None and len(clip_intensity_values) == 2:
            pp_image.clip_intensities(clip_intensity_values[0],
                                      clip_intensity_values[1])  # Clip intensity values in the given range
            pp_image.min_max_normalization()  # Normalize in [0-1] range
        if threshold:
            pp_image.zero_threshold()
        if target_spacing:
            pp_image.resample_spacing(target_spacing=target_spacing)  # Resample image getting 1mm3 voxels
        if fixed_size_pad:
            pp_image.convert_to_fixed_size(fixed_size_pad)
        if largest_cc:
            pp_image.keep_largest_cc()

        if output_folder:
            saved_path = pp_image.save_file()  # Saves the file in output folder
            print("\n   Preprocessed image saved in {}.".format(saved_path))
            if random_blank_patch:
                pp_image.crop_random_blank_patch()
                pp_image.f_name = pp_image.f_name.replace(".nii.gz", "_crop.nii.gz")
                saved_path_cropped = pp_image.save_file()  # Saves the file in output folder
                print("\n   Preprocessed cropped image saved in {}.".format(saved_path_cropped))

        else:
            print("done.")
            return pp_image.image  # Return SimpleITK image
    else:
        print(("  Image file {} not found".format(image_path)))

    if os.path.exists(mask_path):  # Preprocess the mask file (slightly different than the image preprocessing)
        print(("  Mask: {}".format(mask_path)))
        pp_mask = Preprocessor(is_mask=True)
        pp_mask.open(mask_path)  # Load the file
        if register:
            if reg_param_file:
                pp_mask.register_selastix(par_map=reg_param_file)
        if target_spacing:
            pp_mask.resample_spacing(target_spacing=target_spacing)  # Resize the image proportionately to 80 pixels
        if fixed_size_pad:
            pp_mask.convert_to_fixed_size(fixed_size_pad)
        if output_folder:
            pp_mask.set_save_path(output_folder)  # Create output folder if not exists
            saved_path = pp_mask.save_file()  # Saves the file in output folder
            print("\n   Preprocessed mask saved in {}.".format(saved_path))
        else:
            print("done.")
            return pp_mask.image  # Return SimpleITK image
    else:
        print(("  Mask file {} not found.\n".format(mask_path)))


def preprocess_ct_files(input_file_or_folder, output_folder, target_spacing=None, clip_intensity_values=None,
                        image_extension=".nii.gz", image_identifier='image', mask_identifier=None,
                        overwrite_files=False, fixed_size_pad=None, include_subfolders=True, threshold=False,
                        largest_cc=False, register=False, random_blank_patch=False, binary=False, parameter_map=None,
                        atlas_path=None):
    """
    Method for preprocessing the images in a folder (and it subfolders)

    :param input_file_or_folder: Input images. It could be a directory (files and folders to preprocess) or a CSV file of the format:

        image_identifier,mask_identifier
        file001_image.nii.gz,file001_head_mask.nii.gz
        ...

    :param output_folder: Output directory.
    :param target_spacing: Resample image to the spacing provided as parameter.
    :param clip_intensity_values: Clip intensity values to the provided range (default is None).
    :param image_extension: File extension for the images. By default .nii.gz. It could be a list of strings
    :param image_identifier: Part of the filename that indicates it is an image.
    :param mask_identifier: Part of the filename that indicates it is a mask.
    :param fixed_size_pad: zero-pad the output to the size provided in this list.
    :param overwrite_files: overwrite the output files if exist.
    :param include_subfolders: Preprocess also subfolders.
    :param threshold: Apply threshold to non binary images.
    :param largest_cc: Keep the largest connected component in the image.
    :param register: Apply rigid registration to images.

    """
    print("Preprocessing...")
    if overwrite_files:
        print(" Overwrite output files flag is ON.")

    # Decide if preprocess a folder or the files in the csv
    filelist = [(image_identifier, mask_identifier)]  # header
    if os.path.isfile(input_file_or_folder):
        if input_file_or_folder.lower().endswith(".csv"):  # CSV with image paths
            print(("  - Input file: {}".format(input_file_or_folder)))
            print("preprocessing from a csv is not yet supported.")
            return None
        if os.path.splitext(input_file_or_folder)[1] in image_extension:  # Single image
            if mask_identifier:
                names = (input_file_or_folder, input_file_or_folder.replace(image_identifier, mask_identifier))
            else:
                names = (input_file_or_folder, '')
            filelist.append(names)
    else:  # Folder with images
        print((" Input folder: {}".format(input_file_or_folder)))
        for root, dirs, files in os.walk(input_file_or_folder):  # Files and subfolders
            if root == output_folder:  # Avoid output folder.
                continue

            if not include_subfolders and root != input_file_or_folder:
                continue

            for i, name in enumerate(sorted(files, key=len)):
                if (os.path.splitext(name)[1] in image_extension) and (image_identifier in name):
                    filepath = os.path.join(root, name)  # Reconstruct file path.
                    if mask_identifier:
                        names = (filepath, filepath.replace(image_identifier, mask_identifier))
                    else:
                        names = (filepath, '')
                    filelist.append(names)
                else:
                    continue  # Not an image file

    for image_path, mask_path in filelist[1:]:  # First element is the header
        filename = os.path.split(image_path)[1]
        outFile = os.path.join(output_folder, filename)
        if os.path.exists(outFile) and not overwrite_files:  # Useful for resuming a stopped batch preprocessing.
            print(("File {} already preprocessed. Delete the file/containing folder ({}) for processing again.".format(
                filename, output_folder
            )))
            continue

        prep_image_or_mask(image_path, output_folder, mask_path, clip_intensity_values, target_spacing, fixed_size_pad,
                           threshold, largest_cc, register, random_blank_patch, binary, parameter_map, atlas_path)


def prep_img_s(input_ff, out_folder, image_identifier='image', mask_identifier='head_mask', generate_csv=True):
    """ Preprocess a single file or a folder for segmentation purposes.

    :param input_ff: File, folder, or CSV file with the images to be preprocessed.
    :param out_folder:  output folder.
    :param generate_csv:  Generate csv for training a NN flag.
    :return:
    """
    preprocess_ct_files(input_ff, out_folder, clip_intensity_values=[20, 150], target_spacing=[1, 1, 5],
                        overwrite_files=True, fixed_size_pad=[48, 276, 280], include_subfolders=False)

    if generate_csv is True:  # Create a csv file of the preprocessed files
        csv_path, splits = common.create_csv(out_folder, splits=[.7, .1, .2], image_identifier=image_identifier,
                                             mask_identifier=mask_identifier)
        return out_folder, csv_path, splits

    return out_folder


def prep_img_cr(input_ff, out_folder, image_identifier='image', mask_identifier='head_mask', generate_csv=True,
                fixed_pad=None):
    """ Preprocess a single file or a folder for craniectomy reconstruction.

    :param input_ff: File, folder, or CSV file with the images to be preprocessed.
    :param out_folder:  output folder.
    :param generate_csv:  Generate csv for training a NN flag.
    :return:
    """
    preprocess_ct_files(input_ff, out_folder, clip_intensity_values=[90, 91], target_spacing=[2, 2, 2],
                        overwrite_files=True, include_subfolders=False, threshold=True, largest_cc=True,
                        register=True, fixed_size_pad=fixed_pad, random_blank_patch=False)

    if generate_csv is True:  # Create a csv file of the preprocessed files
        csv_path, splits = common.create_csv(out_folder, splits=[.7, .1, .2], image_identifier=image_identifier,
                                             mask_identifier=mask_identifier)
        return out_folder, csv_path, splits

    return out_folder


def prep_cmd():
    parser = argparse.ArgumentParser(description=textwrap.dedent('''\
         Preprocess folder
         --------------------------------
             Preprocess folder for one of the following two cases
             - Segmentation problem (-s argument).
             - Skull reconstruction problem  (-r argument).

               You can't set both -s and -r agruments at the same time.
         '''), formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-s', '--segmentation', action='store_true', help="Segmentation flag")
    parser.add_argument('-r', '--reconstruction', action='store_true', help="Skull reconstruction flag")
    parser.add_argument('-f', type=str, nargs=1, dest='folder', help="Input folder")
    parser.add_argument('-i', type=str, nargs=1, dest='image_id',
                        help="Image identifier (optional, by default 'image')")
    parser.add_argument('-m', type=str, nargs=1, dest='mask_id',
                        help="Mask identifier (optional, by default 'head_mask')")
    parser.add_argument('-o', type=str, nargs=1, dest='output', help="Output folder (optional)")

    args = parser.parse_args()

    if args.segmentation and args.reconstruction:
        print("You have set both -s and -r flags. Select only one of them.")
        return
    elif not args.segmentation and not args.reconstruction:
        print("You have to set a flag to continue. See help for more info.")
        return

    if not args.folder[0]:
        print("You have to set an input folder (-f argument)")
        return

    out_folder = args.output[0] if args.output else os.path.join(args.folder[0], 'preprocessed')
    common.veri_folder(out_folder)

    im_id = args.image_id[0] if args.image_id else 'image'
    mk_id = args.mask_id[0] if args.mask_id else 'head_mask'

    if args.segmentation:
        prep_img_s(args.folder[0], out_folder, im_id, mk_id)
    if args.reconstruction:
        prep_img_cr(args.folder[0], out_folder, im_id, mk_id)


if __name__ == "__main__":
    prep_cmd()
