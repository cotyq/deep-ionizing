# import torchio
import common as cmf
import torch
from skimage.transform import resize
import numpy as np

# class FlapRecTransformTio(torchio.transforms.Transform):
#     """ Transform used for extracting bone flaps. Given an sample, which contains the same torchio Image in the 'image'
#         and 'segmentation', it simulates a bone flap extraction by removing a flap from 'image' placing it in
#         'segmentation'.
#     """
#     def apply_transform(self, sample: torchio.Subject):
#         input_cropped, flap = cmf.random_blank_patch_wrapper(sample['image'][torchio.DATA], prob=1,
#                                                              return_extracted=True)
#         while not flap.max():
#             input_cropped, flap = cmf.random_blank_patch_wrapper(sample['image'][torchio.DATA], prob=1,
#                                                                  return_extracted=True)
#
#         sample['image'][torchio.DATA] = torch.tensor(input_cropped, dtype=torch.float32)
#         sample['segmentation'][torchio.DATA] = torch.tensor(flap, dtype=torch.float32)
#
#         return sample
#
#
# class SkullRecTransformTio(torchio.transforms.Transform):
#     """ Transform used for extracting bone flaps. Given an sample, which contains the same torchio Image in the 'image'
#         and 'segmentation', it simulates a bone flap extraction by removing a flap from 'image' placing it in
#         'segmentation'.
#     """
#     def apply_transform(self, sample: torchio.Subject):
#         sample['segmentation'][torchio.DATA] = sample['image'][torchio.DATA].clone()
#
#         input_cropped, flap = cmf.random_blank_patch_wrapper(sample['image'][torchio.DATA], prob=1,
#                                                              return_extracted=True)
#         while not flap.max():
#             input_cropped, flap = cmf.random_blank_patch_wrapper(sample['image'][torchio.DATA], prob=1,
#                                                                  return_extracted=True)
#
#         sample['image'][torchio.DATA] = torch.tensor(input_cropped, dtype=torch.float32)
#
#         return sample
#

class FlapRecTransform:
    def __new__(cls, img=None):
        if img is not None:
            return cls.apply_transform(img)

    def apply_transform(self, sample):
        sample['image'], sample['target'] = cmf.random_blank_patch_wrapper(sample['image'], prob=0.8, return_extracted=True)
        sample['image'] = cmf.salt_and_pepper(sample['image'], noise_probability=.01, noise_density=.05)
        sample['target'] = torch.tensor(sample['target'], dtype=torch.float32)
        sample['image'] = torch.tensor(sample['image'], dtype=torch.float32)
        sample['target'] = cmf.one_hot_encoding(sample['target'])  # Encode the target
        sample['image'] = sample['image'].unsqueeze(1)  # Add the channel dimension

        return sample

def flap_rec_transform(sample):
    sample['image'] = sample['image'].unsqueeze(0)
    sample['image'], sample['target'] = cmf.random_blank_patch_wrapper(sample['image'], prob=.8, return_extracted=True)
    sample['image'] = cmf.salt_and_pepper(sample['image'], noise_probability=.01, noise_density=.05)
    sample['target'] = torch.tensor(sample['target'], dtype=torch.float32)
    sample['image'] = torch.tensor(sample['image'], dtype=torch.float32)
    sample['target'] = cmf.one_hot_encoding(sample['target']).squeeze(0)  # Encode the target

    return sample
