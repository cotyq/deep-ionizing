# Models used in https://arxiv.org/abs/2007.03817
#   - DE-UNet: recAE_v2_fixed
#   - DE-AE: recAE_noCATv2_fixed
#   - RS-UNet: recAE_v2_fixed

# Other models:
#   - U-Net (img -> segm): UNetF
#   - ACNN regularization autoencoder (segm -> segm): aeACNN
#   - U-Net (segm -> segm) with FC layer in the middle: UNet4FC

import torch.nn as nn
import torch.nn.functional as F
from torch import cat, sigmoid
from torch.utils.checkpoint import checkpoint


def down_block_cr(in_c, out_c, kern_s, pad, dropout_p=0.5):
    """ Encoding block used in the skull reconstruction models.

    :param in_c: Input channels (image).
    :param out_c: Output channels (feature maps).
    :param kern_s: Kernel size.
    :param pad: Padding used in convolutions.
    :param dropout_p: Dropout probability.
    :return: Initialized encoding block.
    """
    return nn.Sequential(nn.Conv3d(in_c, out_c, kernel_size=kern_s, padding=pad),
                         nn.BatchNorm3d(out_c),
                         nn.ReLU(True),
                         nn.Conv3d(out_c, out_c, kernel_size=kern_s, padding=pad),
                         nn.BatchNorm3d(out_c),
                         nn.ReLU(True),
                         nn.Dropout3d(dropout_p))


def up_block_cr(in_c, out_c, kern_s_conv, kern_s_uconv, pad, stride_uc, dropout_p=0.5):
    """ Decoding block used in the skull reconstruction models.

    :param in_c: Input channels (image).
    :param out_c: Output channels (feature maps).
    :param kern_s_conv: Kernel size used in convolutions.
    :param kern_s_uconv: Kernel size used in upconvolutions.
    :param pad: Padding used in convolutions.
    :param stride_uc: Stride used in upconvolutions.
    :param dropout_p: Dropout probability.
    :return: Initialized encoding block.
    """
    return nn.Sequential(nn.ConvTranspose3d(in_c, in_c, kernel_size=kern_s_uconv, stride=stride_uc),
                         nn.Conv3d(in_c, out_c, kernel_size=kern_s_conv, padding=pad),
                         nn.BatchNorm3d(out_c),
                         nn.ReLU(True),
                         nn.Conv3d(out_c, out_c, kernel_size=kern_s_conv, padding=pad),
                         nn.BatchNorm3d(out_c),
                         nn.ReLU(True),
                         nn.Dropout3d(dropout_p))


class UNet4FC(nn.Module):
    """ U-Net model consisting in 4 encoding/decoding blocks and a fully-connected layer in the middle. Unfortunately,
        It's size-dependant of the input (after setting a proper input size, you should set the fully connected input
        manually.
    """

    def __init__(self, input_channels=1, kern_sz_conv=5, kern_sz_upconv=2, stride_upconv=2, i_size=10, padding=2,
                 dropout_p=0,
                 fc_input=3920):
        """ Constructor of the UNet4FC Class

        :param input_channels: Amount of channels in the input (binary image by default).
        :param kern_sz_conv: Kernel size of the convolutions.
        :param kern_sz_upconv: Kernel size of the upconvolutions.
        :param stride_upconv: Stride of the upconvolutions.
        :param i_size: Initial amount of feature maps.
        :param padding: Padding used in up/down convolutions
        :param dropout_p: Dropout p.
        :param fc_input: Input neurons of the FC layer.
        """
        super(UNet4FC, self).__init__()

        n_feat = [i_size * pow(2, n) for n in range(5)]  # if i_size is 10 --> [10, 20, 40, 80]

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1, return_indices=True)

        self.dblock1 = down_block_cr(input_channels, n_feat[0], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(n_feat[0], n_feat[1], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(n_feat[1], n_feat[2], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)
        self.dblock4 = down_block_cr(n_feat[2], n_feat[3], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(n_feat[3], n_feat[4], kernel_size=kern_sz_conv, padding=padding),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.LeakyReLU(True),
                                           nn.Conv3d(n_feat[4], n_feat[4], kernel_size=kern_sz_conv, padding=padding),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.LeakyReLU(True),
                                           nn.Dropout3d(dropout_p))

        inp = fc_input * i_size
        code_size = 256
        print("FC block: {}->{}".format(inp, code_size))

        self.fcblock = nn.Sequential(nn.Linear(inp, code_size),
                                     nn.Linear(code_size, inp),
                                     nn.LeakyReLU(True),
                                     nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(n_feat[4], n_feat[3], kern_sz_conv, kern_sz_upconv, padding, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock2 = up_block_cr(2 * n_feat[3], n_feat[2], kern_sz_conv, kern_sz_upconv, padding, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock3 = up_block_cr(2 * n_feat[2], n_feat[1], kern_sz_conv, kern_sz_upconv, padding, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock4 = up_block_cr(2 * n_feat[1], n_feat[0], kern_sz_conv, kern_sz_upconv, padding, stride_upconv,
                                   dropout_p=dropout_p)

        self.last_conv = nn.Conv3d(2 * n_feat[0], 2, kernel_size=1)

    def forward(self, x):
        o_shape = x.shape  # (batch_size, channels, x, y, z)

        down1 = self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = self.dblock2(down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = self.dblock3(down2_mp)
        down3_mp, _ = self.mp(down3)
        down4 = self.dblock4(down3_mp)
        down4_mp, _ = self.mp(down4)

        cblock = self.cblock_center(down4_mp)
        shape_fc = list(cblock.shape)  # Shape before the converting the tensor to a two dimensional array
        fc_input = cblock.view(o_shape[0], -1)

        fc_out = self.fcblock(fc_input)
        fc_out_rshape = fc_out.view(shape_fc)

        up1 = self.ublock1(fc_out_rshape)
        up1 = cat((up1, down4), 1)
        up2 = self.ublock2(up1)
        up2 = cat((up2, down3), 1)
        up3 = self.ublock3(up2)
        up3 = cat((up3, down2), 1)
        up4 = self.ublock4(up3)
        up4 = cat((up4, down1), 1)
        lc = self.last_conv(up4)

        return F.softmax(sigmoid(lc), dim=1)


class recAE_v2_fixed(nn.Module):
    """ U-Net model consisting in 4 encoding/decoding blocks.
    """

    def __init__(self, input_channels=1, kern_sz_conv=5, kern_sz_upconv=2, stride_upconv=2, i_size=8, padding=2,
                 dropout_p=0, use_checkpoint=True):
        """ Constructor of the recAE_v2_fixed Class. This is a fully convolutional model, so it can be trained with any
            input size (be aware that since it produce 4 max-poolings, the size should be multiple of 16)

        :param input_channels: Amount of channels in the input (binary image by default).
        :param kern_sz_conv: Kernel size of the convolutions.
        :param kern_sz_upconv: Kernel size of the upconvolutions.
        :param stride_upconv: Stride of the upconvolutions.
        :param i_size: Initial amount of feature maps.
        :param padding: Padding used in up/down convolutions
        :param dropout_p: Dropout p.
        """
        super(recAE_v2_fixed, self).__init__()

        self.use_checkpoint = use_checkpoint

        fms = [i_size * pow(2, n) for n in range(5)]  # Feature maps sizes

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1, return_indices=True)

        self.dblock1 = down_block_cr(input_channels, fms[0], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(fms[0], fms[1], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(fms[1], fms[2], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)
        self.dblock4 = down_block_cr(fms[2], fms[3], kern_s=kern_sz_conv, pad=padding, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(fms[3], fms[4], kernel_size=kern_sz_conv, padding=padding),
                                           nn.BatchNorm3d(fms[4]),
                                           nn.ReLU(True),
                                           nn.Conv3d(fms[4], fms[4], kernel_size=kern_sz_conv, padding=padding),
                                           nn.BatchNorm3d(fms[4]),
                                           nn.ReLU(True),
                                           nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(fms[4], fms[3], kern_sz_conv, kern_sz_upconv, padding, stride_upconv, dropout_p)
        self.ublock2 = up_block_cr(2 * fms[3], fms[2], kern_sz_conv, kern_sz_upconv, padding, stride_upconv, dropout_p)
        self.ublock3 = up_block_cr(2 * fms[2], fms[1], kern_sz_conv, kern_sz_upconv, padding, stride_upconv, dropout_p)
        self.ublock4 = up_block_cr(2 * fms[1], fms[0], kern_sz_conv, kern_sz_upconv, padding, stride_upconv, dropout_p)

        self.last_conv = nn.Conv3d(2 * fms[0], 2, kernel_size=1)

    def forward(self, x):
        down1 = checkpoint(self.dblock1, x) if self.use_checkpoint else self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = checkpoint(self.dblock2, down1_mp) if self.use_checkpoint else self.dblock2(down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = checkpoint(self.dblock3, down2_mp) if self.use_checkpoint else self.dblock3(down2_mp)
        down3_mp, _ = self.mp(down3)
        down4 = checkpoint(self.dblock4, down3_mp) if self.use_checkpoint else self.dblock4(down3_mp)
        down4_mp, _ = self.mp(down4)

        cblock = checkpoint(self.cblock_center, down4_mp) if self.use_checkpoint else self.cblock_center(down4_mp)

        up1 = checkpoint(self.ublock1, cblock) if self.use_checkpoint else self.ublock1(cblock)
        up1 = cat((up1, down4), 1)
        up2 = checkpoint(self.ublock2, up1) if self.use_checkpoint else self.ublock2(up1)
        up2 = cat((up2, down3), 1)
        up3 = checkpoint(self.ublock3, up2) if self.use_checkpoint else self.ublock3(up2)
        up3 = cat((up3, down2), 1)
        up4 = checkpoint(self.ublock4, up3) if self.use_checkpoint else self.ublock4(up3)
        up4 = cat((up4, down1), 1)
        lc = checkpoint(self.last_conv, up4) if self.use_checkpoint else self.last_conv(up4)

        return F.softmax(lc, dim=1)

class UNet4_2IC(recAE_v2_fixed):
    def __init__(self):
        """ Constructor of the recAE_v2_fixed Class. This is a fully convolutional model, so it can be trained with any
            input size (be aware that since it produce 4 max-poolings, the size should be multiple of 16)

        :param input_channels: Amount of channels in the input (binary image by default).
        :param kern_sz_conv: Kernel size of the convolutions.
        :param kern_sz_upconv: Kernel size of the upconvolutions.
        :param stride_upconv: Stride of the upconvolutions.
        :param i_size: Initial amount of feature maps.
        :param padding: Padding used in up/down convolutions
        :param dropout_p: Dropout p.
        """
        super(UNet4_2IC, self).__init__(i_size=7, input_channels=2)

class AutoimplantNet(nn.Module):
    """ Autoencoder used for learning a small representation of CT scans without craniectomy and after used to
        reconstruct missing skull of scans of patients with craniectomy.

    """

    def __init__(self):
        super(AutoimplantNet, self).__init__()

        kern_sz_conv = 5
        kern_sz_upconv = 2
        stride_upconv = 2

        i_size = 5
        n_feat = [i_size * pow(2, n) for n in range(4)]
        pad = 2
        dropout_p = 0

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1,
                               return_indices=True)  # Reduces size by half

        self.dblock1 = down_block_cr(1, n_feat[0], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(n_feat[0], n_feat[1], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(n_feat[1], n_feat[2], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(n_feat[2], n_feat[3], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[3]),
                                           nn.ReLU(True),
                                           nn.Conv3d(n_feat[3], n_feat[3], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[3]),
                                           nn.ReLU(True),
                                           nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(n_feat[3], n_feat[2], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock2 = up_block_cr(2 * n_feat[2], n_feat[1], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock3 = up_block_cr(2 * n_feat[1], n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)

        self.last_conv = nn.Conv3d(2 * n_feat[0], 2, kernel_size=1)

    def forward(self, x):
        down1 = self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = checkpoint(self.dblock2, down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = checkpoint(self.dblock3, down2_mp)
        down3_mp, _ = self.mp(down3)

        cblock = checkpoint(self.cblock_center, down3_mp)

        up1 = checkpoint(self.ublock1, cblock)
        up1 = cat((up1, down3), 1)
        up2 = checkpoint(self.ublock2, up1)
        up2 = cat((up2, down2), 1)
        up3 = checkpoint(self.ublock3, up2)
        up3 = cat((up3, down1), 1)
        lc = checkpoint(self.last_conv, up3)

        return F.softmax(lc, dim=1)


class UNetVariableModel(nn.Module):
    def __init__(self, in_channels=1, out_channels=2, blocks_count=4, kern_sz_conv=5, kern_sz_upconv=2, stride_upconv=2,
                 fm_init_size=5,
                 conv_padding=2, dropout_p=0):
        super(UNetVariableModel, self).__init__()

        fms = [fm_init_size * pow(2, n) for n in range(blocks_count + 1)]  # Feature maps sizes

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1, return_indices=True)

        self.dblock1 = down_block_cr(in_channels, fms[0], kern_s=kern_sz_conv, pad=conv_padding, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(fms[0], fms[1], kern_s=kern_sz_conv, pad=conv_padding, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(fms[1], fms[2], kern_s=kern_sz_conv, pad=conv_padding, dropout_p=dropout_p)
        self.dblock4 = down_block_cr(fms[2], fms[3], kern_s=kern_sz_conv, pad=conv_padding, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(fms[3], fms[4], kernel_size=kern_sz_conv, padding=conv_padding),
                                           nn.BatchNorm3d(fms[4]),
                                           nn.ReLU(True),
                                           nn.Conv3d(fms[4], fms[4], kernel_size=kern_sz_conv, padding=conv_padding),
                                           nn.BatchNorm3d(fms[4]),
                                           nn.ReLU(True),
                                           nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(fms[4], fms[3], kern_sz_conv, kern_sz_upconv, conv_padding, stride_upconv, dropout_p)
        self.ublock2 = up_block_cr(2 * fms[3], fms[2], kern_sz_conv, kern_sz_upconv, conv_padding, stride_upconv,
                                   dropout_p)
        self.ublock3 = up_block_cr(2 * fms[2], fms[1], kern_sz_conv, kern_sz_upconv, conv_padding, stride_upconv,
                                   dropout_p)
        self.ublock4 = up_block_cr(2 * fms[1], fms[0], kern_sz_conv, kern_sz_upconv, conv_padding, stride_upconv,
                                   dropout_p)

        self.last_conv = nn.Conv3d(2 * fms[0], out_channels, kernel_size=1)

    def forward(self, x):
        down1 = self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = self.dblock2(down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = self.dblock3(down2_mp)
        down3_mp, _ = self.mp(down3)
        down4 = self.dblock4(down3_mp)
        down4_mp, _ = self.mp(down4)

        cblock = self.cblock_center(down4_mp)

        up1 = self.ublock1(cblock)
        up1 = cat((up1, down4), 1)
        up2 = self.ublock2(up1)
        up2 = cat((up2, down3), 1)
        up3 = self.ublock3(up2)
        up3 = cat((up3, down2), 1)
        up4 = self.ublock4(up3)
        up4 = cat((up4, down1), 1)
        lc = self.last_conv(up4)

        return F.softmax(lc, dim=1)


# class AutoImplant_Net(nn.Module):
#     """ Autoencoder used for learning a small representation of CT scans without craniectomy and after used to
#         reconstruct missing skull of scans of patients with craniectomy.
#
#     """
#
#     def __init__(self):
#         super(AutoImplant_Net, self).__init__()
#
#         kern_sz_conv = 5
#         kern_sz_upconv = 2
#         stride_upconv = 2
#
#         i_size = 2
#         n_feat = [2, 2, 2, 2, 2]
#         pad = 2
#         dropout_p = 0
#
#         self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1,
#                                return_indices=True)  # Reduces size by half
#
#         self.dblock1 = down_block_cr(1, n_feat[0], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
#         self.dblock2 = down_block_cr(n_feat[0], n_feat[1], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
#         self.dblock3 = down_block_cr(n_feat[1], n_feat[2], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
#         self.dblock4 = down_block_cr(n_feat[2], n_feat[3], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
#
#         self.cblock_center = nn.Sequential(nn.Conv3d(n_feat[3], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
#                                            nn.BatchNorm3d(n_feat[4]),
#                                            nn.ReLU(True),
#                                            nn.Conv3d(n_feat[4], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
#                                            nn.BatchNorm3d(n_feat[4]),
#                                            nn.ReLU(True),
#                                            nn.Dropout3d(dropout_p))
#
#         self.ublock1 = up_block_cr(n_feat[4], n_feat[3], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
#                                    dropout_p=dropout_p)
#         self.ublock2 = up_block_cr(2 * n_feat[3], n_feat[2], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
#                                    dropout_p=dropout_p)
#         self.ublock3 = up_block_cr(2 * n_feat[2], n_feat[1], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
#                                    dropout_p=dropout_p)
#         self.ublock4 = up_block_cr(2 * n_feat[1], n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
#                                    dropout_p=dropout_p)
#         self.ublock5 = up_block_cr(2 * n_feat[0], n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
#                                    dropout_p=dropout_p)
#         self.ublock6 = up_block_cr(3, n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
#                                    dropout_p=dropout_p)
#
#         self.last_conv = nn.Conv3d(3, 2, kernel_size=1)
#
#     def forward(self, x):
#         down00_mp, _ = self.mp(x)
#         down01_mp, _ = self.mp(down00_mp)
#
#         down1 = self.dblock1(down01_mp)
#         down1_mp, _ = self.mp(down1)
#         down2 = self.dblock2(down1_mp)
#         down2_mp, _ = self.mp(down2)
#         down3 = self.dblock3(down2_mp)
#         down3_mp, _ = self.mp(down3)
#         down4 = self.dblock4(down3_mp)
#         down4_mp, _ = self.mp(down4)
#
#         cblock = self.cblock_center(down4_mp)
#
#         up1 = self.ublock1(cblock)
#         up1 = cat((up1, down4), 1)
#         up2 = self.ublock2(up1)
#         up2 = cat((up2, down3), 1)
#         up3 = self.ublock3(up2)
#         up3 = cat((up3, down2), 1)
#         up4 = self.ublock4(up3)
#         up4 = cat((up4, down1), 1)
#         up5 = self.ublock5(up4)
#         up5 = cat((up5, down00_mp), 1)
#         up6 = self.ublock6(up5)
#         up6 = cat((up6, x), 1)
#         lc = self.last_conv(up6)
#
#         return F.softmax(lc, dim=1)
#

class recAE_v3(nn.Module):
    """ Autoencoder used for learning a small representation of CT scans without craniectomy and after used to
        reconstruct missing skull of scans of patients with craniectomy. This model is smaller than recAE_v2.

    """

    def __init__(self):
        super(recAE_v3, self).__init__()

        kern_sz_conv = 5
        kern_sz_upconv = 2
        stride_upconv = 2

        i_size = 5
        n_feat = [i_size * pow(2, n) for n in range(5)]
        pad = 2
        dropout_p = 0

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1,
                               return_indices=True)  # Reduces size by half

        self.dblock1 = down_block_cr(1, n_feat[0], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(n_feat[0], n_feat[1], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(n_feat[1], n_feat[2], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock4 = down_block_cr(n_feat[2], n_feat[3], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(n_feat[3], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.LeakyReLU(True),
                                           nn.Conv3d(n_feat[4], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.LeakyReLU(True),
                                           nn.Dropout3d(dropout_p))

        inp = 3920 * i_size
        code_size = 1024
        print("FC block: {}->{}".format(inp, code_size))

        self.fcblock = nn.Sequential(nn.Linear(inp, code_size),
                                     nn.Linear(code_size, inp),
                                     nn.LeakyReLU(True),
                                     nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(n_feat[4], n_feat[3], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock2 = up_block_cr(2 * n_feat[3], n_feat[2], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock3 = up_block_cr(2 * n_feat[2], n_feat[1], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock4 = up_block_cr(2 * n_feat[1], n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)

        self.last_conv = nn.Conv3d(2 * n_feat[0], 2, kernel_size=1)

    def forward(self, x):
        o_shape = x.shape  # (batch_size, channels, x, y, z)

        down1 = self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = self.dblock2(down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = self.dblock3(down2_mp)
        down3_mp, _ = self.mp(down3)
        down4 = self.dblock4(down3_mp)
        down4_mp, _ = self.mp(down4)

        cblock = self.cblock_center(down4_mp)
        shape_fc = list(cblock.shape)  # Shape before the converting the tensor to a two dimensional array
        fc_input = cblock.view(o_shape[0], -1)

        fc_out = self.fcblock(fc_input)
        fc_out_rshape = fc_out.view(shape_fc)

        up1 = self.ublock1(fc_out_rshape)
        up1 = cat((up1, down4), 1)
        up2 = self.ublock2(up1)
        up2 = cat((up2, down3), 1)
        up3 = self.ublock3(up2)
        up3 = cat((up3, down2), 1)
        up4 = self.ublock4(up3)
        up4 = cat((up4, down1), 1)
        lc = self.last_conv(up4)

        return F.softmax(sigmoid(lc), dim=1)


class recAE_noCAT(nn.Module):
    """ Autoencoder used for learning a small representation of CT scans without craniectomy and after used to
        reconstruct missing skull of scans of patients with craniectomy. This model is equal to recAE_v3 but without skip
        connections.

    """

    def __init__(self):
        super(recAE_noCAT, self).__init__()

        kern_sz_conv = 5
        kern_sz_upconv = 2
        stride_upconv = 2

        i_size = 5
        n_feat = [i_size * pow(2, n) for n in range(5)]
        pad = 2
        dropout_p = 0.3

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1,
                               return_indices=True)  # Reduces size by half

        self.dblock1 = down_block_cr(1, n_feat[0], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(n_feat[0], n_feat[1], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(n_feat[1], n_feat[2], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock4 = down_block_cr(n_feat[2], n_feat[3], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(n_feat[3], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.ReLU(True),
                                           nn.Conv3d(n_feat[4], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.ReLU(True),
                                           nn.Dropout3d(dropout_p))

        inp = 3920 * i_size
        code_size = 512
        print("FC block: {}->{}".format(inp, code_size))

        self.fcblock = nn.Sequential(nn.Linear(inp, code_size),
                                     nn.Linear(code_size, inp),
                                     nn.ReLU(True),
                                     nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(n_feat[4], n_feat[3], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock2 = up_block_cr(n_feat[3], n_feat[2], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock3 = up_block_cr(n_feat[2], n_feat[1], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock4 = up_block_cr(n_feat[1], n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)

        self.last_conv = nn.Conv3d(n_feat[0], 2, kernel_size=1)

    def forward(self, x):
        o_shape = x.shape  # (batch_size, channels, x, y, z)

        down1 = self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = self.dblock2(down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = self.dblock3(down2_mp)
        down3_mp, _ = self.mp(down3)
        down4 = self.dblock4(down3_mp)
        down4_mp, _ = self.mp(down4)

        cblock = self.cblock_center(down4_mp)
        shape_fc = list(cblock.shape)  # Shape before the converting the tensor to a two dimensional array
        fc_input = cblock.view(o_shape[0], -1)

        fc_out = self.fcblock(fc_input)
        fc_out_rshape = fc_out.view(shape_fc)

        up1 = self.ublock1(fc_out_rshape)
        up2 = self.ublock2(up1)
        up3 = self.ublock3(up2)
        up4 = self.ublock4(up3)
        lc = self.last_conv(up4)

        return F.softmax(sigmoid(lc), dim=1)


class recAE_noCATv2(nn.Module):
    """ Autoencoder used for learning a small representation of CT scans without craniectomy and after used to
        reconstruct missing skull of scans of patients with craniectomy. This model is equal to recAE_v2 but without skip
        connections.

    """

    def __init__(self):
        super(recAE_noCATv2, self).__init__()

        kern_sz_conv = 5
        kern_sz_upconv = 2
        stride_upconv = 2

        i_size = 10
        n_feat = [i_size * pow(2, n) for n in range(5)]
        pad = 2
        dropout_p = 0

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1,
                               return_indices=True)  # Reduces size by half

        self.dblock1 = down_block_cr(1, n_feat[0], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(n_feat[0], n_feat[1], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(n_feat[1], n_feat[2], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock4 = down_block_cr(n_feat[2], n_feat[3], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(n_feat[3], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.LeakyReLU(True),
                                           nn.Conv3d(n_feat[4], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.LeakyReLU(True),
                                           nn.Dropout3d(dropout_p))

        inp = 3920 * i_size
        code_size = 256
        print("FC block: {}->{}".format(inp, code_size))

        self.fcblock = nn.Sequential(nn.Linear(inp, code_size),
                                     nn.Linear(code_size, inp),
                                     nn.LeakyReLU(True),
                                     nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(n_feat[4], n_feat[3], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock2 = up_block_cr(n_feat[3], n_feat[2], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock3 = up_block_cr(n_feat[2], n_feat[1], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock4 = up_block_cr(n_feat[1], n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)

        self.last_conv = nn.Conv3d(n_feat[0], 2, kernel_size=1)

    def forward(self, x):
        o_shape = x.shape  # (batch_size, channels, x, y, z)

        down1 = self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = self.dblock2(down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = self.dblock3(down2_mp)
        down3_mp, _ = self.mp(down3)
        down4 = self.dblock4(down3_mp)
        down4_mp, _ = self.mp(down4)

        cblock = self.cblock_center(down4_mp)
        shape_fc = list(cblock.shape)  # Shape before the converting the tensor to a two dimensional array
        fc_input = cblock.view(o_shape[0], -1)

        fc_out = self.fcblock(fc_input)
        fc_out_rshape = fc_out.view(shape_fc)

        up1 = self.ublock1(fc_out_rshape)
        up2 = self.ublock2(up1)
        up3 = self.ublock3(up2)
        up4 = self.ublock4(up3)
        lc = self.last_conv(up4)

        return F.softmax(sigmoid(lc), dim=1)


class recAE_noCATv2_fixed(nn.Module):
    """ Autoencoder used for learning a small representation of CT scans without craniectomy and after used to
        reconstruct missing skull of scans of patients with craniectomy.

    """

    def __init__(self):
        super(recAE_noCATv2_fixed, self).__init__()

        kern_sz_conv = 5
        kern_sz_upconv = 2
        stride_upconv = 2

        i_size = 10
        n_feat = [i_size * pow(2, n) for n in range(5)]
        pad = 2
        dropout_p = 0

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1,
                               return_indices=True)  # Reduces size by half

        self.dblock1 = down_block_cr(1, n_feat[0], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock2 = down_block_cr(n_feat[0], n_feat[1], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock3 = down_block_cr(n_feat[1], n_feat[2], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)
        self.dblock4 = down_block_cr(n_feat[2], n_feat[3], kern_s=kern_sz_conv, pad=pad, dropout_p=dropout_p)

        self.cblock_center = nn.Sequential(nn.Conv3d(n_feat[3], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.ReLU(True),
                                           nn.Conv3d(n_feat[4], n_feat[4], kernel_size=kern_sz_conv, padding=pad),
                                           nn.BatchNorm3d(n_feat[4]),
                                           nn.ReLU(True),
                                           nn.Dropout3d(dropout_p))

        self.ublock1 = up_block_cr(n_feat[4], n_feat[3], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock2 = up_block_cr(n_feat[3], n_feat[2], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock3 = up_block_cr(n_feat[2], n_feat[1], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)
        self.ublock4 = up_block_cr(n_feat[1], n_feat[0], kern_sz_conv, kern_sz_upconv, pad, stride_upconv,
                                   dropout_p=dropout_p)

        self.last_conv = nn.Conv3d(n_feat[0], 2, kernel_size=1)

    def forward(self, x):
        down1 = self.dblock1(x)
        down1_mp, _ = self.mp(down1)
        down2 = self.dblock2(down1_mp)
        down2_mp, _ = self.mp(down2)
        down3 = self.dblock3(down2_mp)
        down3_mp, _ = self.mp(down3)
        down4 = self.dblock4(down3_mp)
        down4_mp, _ = self.mp(down4)

        cblock = self.cblock_center(down4_mp)

        up1 = self.ublock1(cblock)
        up2 = self.ublock2(up1)
        up3 = self.ublock3(up2)
        up4 = self.ublock4(up3)
        lc = self.last_conv(up4)

        return F.softmax(lc, dim=1)


class UNetF(nn.Module):
    """ Modified UNet model (Ronneberger) that uses 3D images, less feature maps (4x less), and with BatchNorm in all
        the layers.

    """

    def __init__(self):
        super(UNetF, self).__init__()
        # Unet:
        kernel_sz = 3
        i_size = 16

        self.batchNormi_1 = nn.BatchNorm3d(i_size, momentum=0.5)
        self.batchNormi_2 = nn.BatchNorm3d(i_size, momentum=0.5)
        self.batchNormi_3 = nn.BatchNorm3d(i_size, momentum=0.5)
        self.batchNormi_4 = nn.BatchNorm3d(i_size, momentum=0.5)
        self.batchNorm2i_1 = nn.BatchNorm3d(i_size * 2, momentum=0.5)
        self.batchNorm2i_2 = nn.BatchNorm3d(i_size * 2, momentum=0.5)
        self.batchNorm2i_3 = nn.BatchNorm3d(i_size * 2, momentum=0.5)
        self.batchNorm2i_4 = nn.BatchNorm3d(i_size * 2, momentum=0.5)
        self.batchNorm4i_1 = nn.BatchNorm3d(i_size * 4, momentum=0.5)
        self.batchNorm4i_2 = nn.BatchNorm3d(i_size * 4, momentum=0.5)
        self.batchNorm4i_3 = nn.BatchNorm3d(i_size * 4, momentum=0.5)
        self.batchNorm4i_4 = nn.BatchNorm3d(i_size * 4, momentum=0.5)
        self.batchNorm4i_5 = nn.BatchNorm3d(i_size * 4, momentum=0.5)
        self.batchNorm4i_6 = nn.BatchNorm3d(i_size * 4, momentum=0.5)
        self.batchNorm8i_1 = nn.BatchNorm3d(i_size * 8, momentum=0.5)
        self.batchNorm8i_2 = nn.BatchNorm3d(i_size * 8, momentum=0.5)
        self.batchNorm8i_3 = nn.BatchNorm3d(i_size * 8, momentum=0.5)
        self.batchNorm8i_4 = nn.BatchNorm3d(i_size * 8, momentum=0.5)
        self.batchNorm16i_1 = nn.BatchNorm3d(i_size * 16, momentum=0.5)
        self.batchNorm16i_2 = nn.BatchNorm3d(i_size * 16, momentum=0.5)

        self.conv1_i = nn.Conv3d(1, i_size, kernel_size=kernel_sz, padding=1)
        self.convi_i_1 = nn.Conv3d(i_size, i_size, kernel_size=kernel_sz, padding=1)

        self.mp = nn.MaxPool3d(2, padding=0, stride=2, dilation=1)

        self.convi_2i = nn.Conv3d(i_size, i_size * 2, kernel_size=kernel_sz, padding=1)
        self.conv2i_2i_1 = nn.Conv3d(i_size * 2, i_size * 2, kernel_size=kernel_sz, padding=1)

        self.conv2i_4i = nn.Conv3d(i_size * 2, i_size * 4, kernel_size=kernel_sz, padding=1)
        self.conv4i_4i_1 = nn.Conv3d(i_size * 4, i_size * 4, kernel_size=kernel_sz, padding=1)

        self.conv4i_8i = nn.Conv3d(i_size * 4, i_size * 8, kernel_size=kernel_sz, padding=1)
        self.conv8i_8i_1 = nn.Conv3d(i_size * 8, i_size * 8, kernel_size=kernel_sz, padding=1)

        self.conv8i_16i = nn.Conv3d(i_size * 8, i_size * 16, kernel_size=kernel_sz, padding=1)
        self.conv16i_16i_1 = nn.Conv3d(i_size * 16, i_size * 16, kernel_size=kernel_sz, padding=1)

        self.upconv16i_8i = nn.ConvTranspose3d(i_size * 16, i_size * 8, 2, stride=2)
        self.conv16i_8i = nn.Conv3d(i_size * 16, i_size * 8, kernel_size=kernel_sz, padding=1)
        self.conv8i_8i_2 = nn.Conv3d(i_size * 8, i_size * 8, kernel_size=kernel_sz, padding=1)

        self.upconv8i_4i = nn.ConvTranspose3d(i_size * 8, i_size * 4, 2, stride=2)
        self.conv8i_4i = nn.Conv3d(i_size * 8, i_size * 4, kernel_size=kernel_sz, padding=1)
        self.conv4i_4i_2 = nn.Conv3d(i_size * 4, i_size * 4, kernel_size=kernel_sz, padding=1)

        self.upconv4i_2i = nn.ConvTranspose3d(i_size * 4, i_size * 2, 2, stride=2)
        self.conv4i_2i = nn.Conv3d(i_size * 4, i_size * 2, kernel_size=kernel_sz, padding=1)
        self.conv2i_2i_2 = nn.Conv3d(i_size * 2, i_size * 2, kernel_size=kernel_sz, padding=1)

        self.upconv2i_i = nn.ConvTranspose3d(i_size * 2, i_size, 2, stride=2)
        self.conv2i_i = nn.Conv3d(i_size * 2, i_size, kernel_size=kernel_sz, padding=1)
        self.conv_i_i_2 = nn.Conv3d(i_size, i_size, kernel_size=kernel_sz, padding=1)
        self.convi_2 = nn.Conv3d(i_size, 2, kernel_size=1)  # 1x1 conv

    def forward(self, x):
        c1 = self.batchNormi_1(self.conv1_i(x))
        r1 = F.relu(c1)
        c2 = self.batchNormi_2(self.convi_i_1(r1))
        r2 = F.relu(c2)
        mp1 = self.mp(r2)  # 1st max-pooling

        c3 = self.batchNorm2i_1(self.convi_2i(mp1))
        r3 = F.relu(c3)
        c4 = self.batchNorm2i_2(self.conv2i_2i_1(r3))
        r4 = F.relu(c4)
        mp2 = self.mp(r4)  # 2nd max-pooling

        c5 = self.batchNorm4i_1(self.conv2i_4i(mp2))
        r5 = F.relu(c5)
        c6 = self.batchNorm4i_2(self.conv4i_4i_1(r5))
        r6 = F.relu(c6)
        mp3 = self.mp(r6)  # 3rd max-pooling

        c7 = self.batchNorm8i_1(self.conv4i_8i(mp3))
        r7 = F.relu(c7)
        c8 = self.batchNorm8i_2(self.conv8i_8i_1(r7))
        r8 = F.relu(c8)
        mp4 = self.mp(r8)  # 4th max-pooling

        c9 = self.batchNorm16i_1(self.conv8i_16i(mp4))
        r9 = F.relu(c9)
        c10 = self.batchNorm16i_2(self.conv16i_16i_1(r9))
        r10 = F.relu(c10)

        uc1 = self.upconv16i_8i(r10)  # 1st upconvolution
        uc1 = cat((uc1, r8), 1)
        c11 = self.batchNorm8i_3(self.conv16i_8i(uc1))
        r11 = F.relu(c11)
        c12 = self.batchNorm8i_4(self.conv8i_8i_2(r11))
        r12 = F.relu(c12)

        uc2 = self.upconv8i_4i(r12)  # 2nd upconvolution
        uc2 = cat((uc2, r6), 1)
        c13 = self.batchNorm4i_3(self.conv8i_4i(uc2))
        r13 = F.relu(c13)
        c14 = self.batchNorm4i_4(self.conv4i_4i_2(r13))
        r14 = F.relu(c14)

        uc3 = self.upconv4i_2i(r14)  # 3rd upconvolution
        uc3 = cat((uc3, r4), 1)
        c15 = self.batchNorm2i_3(self.conv4i_2i(uc3))
        r15 = F.relu(c15)
        c16 = self.batchNorm2i_4(self.conv2i_2i_2(r15))
        r16 = F.relu(c16)

        uc4 = self.upconv2i_i(r16)  # 4th upconvolution
        uc4 = cat((uc4, r2), 1)
        c17 = self.batchNormi_3(self.conv2i_i(uc4))
        r17 = F.relu(c17)
        c18 = self.batchNormi_4(self.conv_i_i_2(r17))
        r18 = F.relu(c18)
        c19 = self.convi_2(r18)

        return F.softmax(sigmoid(c19), dim=1)


class aeACNN(nn.Module):
    """ Denoising autoencoder used in ACNN model to clean the noisy masks and learning a small representation of them,
        for comparing small representations after in the UNet and add that difference as an error.

    """

    def __init__(self, kernel_sz=3):
        super(aeACNN, self).__init__()

        i_size = 8

        self.mp = nn.MaxPool3d(kernel_size=2, padding=0, stride=2, dilation=1,
                               return_indices=True)  # Reduces size by half

        pad = 1

        self.batchNorm_i_1 = nn.BatchNorm3d(i_size)
        self.batchNorm_i_2 = nn.BatchNorm3d(i_size)
        self.batchNorm_i_3 = nn.BatchNorm3d(i_size)
        self.batchNorm_i_4 = nn.BatchNorm3d(i_size)

        self.batchNorm_2i_1 = nn.BatchNorm3d(i_size * 2)
        self.batchNorm_2i_2 = nn.BatchNorm3d(i_size * 2)
        self.batchNorm_2i_3 = nn.BatchNorm3d(i_size * 2)
        self.batchNorm_2i_4 = nn.BatchNorm3d(i_size * 2)

        self.batchNorm_4i_1 = nn.BatchNorm3d(i_size * 4)
        self.batchNorm_4i_2 = nn.BatchNorm3d(i_size * 4)
        self.batchNorm_4i_3 = nn.BatchNorm3d(i_size * 4)
        self.batchNorm_4i_4 = nn.BatchNorm3d(i_size * 4)
        self.batchNorm_4i_5 = nn.BatchNorm3d(i_size * 4)
        self.batchNorm_4i_6 = nn.BatchNorm3d(i_size * 4)

        self.batchNorm_8i_1 = nn.BatchNorm3d(i_size * 8)
        self.batchNorm_8i_2 = nn.BatchNorm3d(i_size * 8)
        self.batchNorm_8i_3 = nn.BatchNorm3d(i_size * 8)
        self.batchNorm_8i_4 = nn.BatchNorm3d(i_size * 8)
        self.batchNorm_8i_5 = nn.BatchNorm3d(i_size * 8)

        self.batchNorm_16i_1 = nn.BatchNorm3d(i_size * 16)
        self.batchNorm_16i_2 = nn.BatchNorm3d(i_size * 16)

        self.conv1_i = nn.Conv3d(1, i_size, kernel_size=kernel_sz, padding=pad)
        self.convi_i = nn.Conv3d(i_size, i_size, kernel_size=kernel_sz, padding=pad)  # Shared
        self.convi_2i = nn.Conv3d(i_size, i_size * 2, kernel_size=kernel_sz, padding=pad)
        self.conv2i_2i = nn.Conv3d(i_size * 2, i_size * 2, kernel_size=kernel_sz, padding=pad)  # Shared
        self.conv2i_4i = nn.Conv3d(i_size * 2, i_size * 4, kernel_size=kernel_sz, padding=pad)
        self.conv4i_4i = nn.Conv3d(i_size * 4, i_size * 4, kernel_size=kernel_sz, padding=pad)  # Shared
        self.conv4i_8i = nn.Conv3d(i_size * 4, i_size * 8, kernel_size=kernel_sz, padding=pad)
        self.conv8i_8i = nn.Conv3d(i_size * 8, i_size * 8, kernel_size=kernel_sz, padding=pad)  # Shared
        self.conv8i_16i = nn.Conv3d(i_size * 8, i_size * 16, kernel_size=kernel_sz, padding=pad)
        self.conv16i_16i = nn.Conv3d(i_size * 16, i_size * 16, kernel_size=kernel_sz, padding=pad)
        self.conv16i_8i = nn.Conv3d(i_size * 16, i_size * 8, kernel_size=kernel_sz, padding=pad)
        self.conv8i_4i = nn.Conv3d(i_size * 8, i_size * 4, kernel_size=kernel_sz, padding=pad)
        self.conv4i_2i = nn.Conv3d(i_size * 4, i_size * 2, kernel_size=kernel_sz, padding=pad)
        self.conv2i_i = nn.Conv3d(i_size * 2, i_size, kernel_size=kernel_sz, padding=pad)
        self.convi_1 = nn.Conv3d(i_size, 1, kernel_size=1)  # 1x1 conv
        self.conv8i_1 = nn.Conv3d(i_size * 8, 1, kernel_size=1)  # 1x1 conv
        self.convi_2 = nn.Conv3d(i_size, 2, kernel_size=1)  # 1x1 conv

        self.upconv16i_16i = nn.ConvTranspose3d(i_size * 16, i_size * 16, kernel_size=2, stride=2)
        self.upconv8i_8i = nn.ConvTranspose3d(i_size * 8, i_size * 8, kernel_size=2, stride=2)
        self.upconv4i_4i = nn.ConvTranspose3d(i_size * 4, i_size * 4, kernel_size=2, stride=2)
        self.upconv2i_2i = nn.ConvTranspose3d(i_size * 2, i_size * 2, kernel_size=2, stride=2)
        self.upconv1_8i = nn.ConvTranspose3d(1, i_size * 8, kernel_size=2, stride=2)

        self.fc1 = nn.Linear(1960, 256)  # Num. kernels = 32 (originally 64)
        self.fc2 = nn.Linear(256, 1960)

    def encode(self, x):
        o_shape = x.shape  # (batch_size, channels, x, y, z)
        c1 = self.batchNorm_i_1(self.conv1_i(x))
        r1 = F.relu(c1)

        c2 = self.batchNorm_i_2(self.convi_i(r1))
        r2 = F.relu(c2)
        mp1, idxi = self.mp(r2)  # 1st max-pooling

        c3 = self.batchNorm_2i_1(self.convi_2i(mp1))
        r3 = F.relu(c3)
        c4 = self.batchNorm_2i_2(self.conv2i_2i(r3))
        r4 = F.relu(c4)
        mp2, idx2i = self.mp(r4)  # 2nd max-pooling

        c5 = self.batchNorm_4i_1(self.conv2i_4i(mp2))
        r5 = F.relu(c5)
        c6 = self.batchNorm_4i_2(self.conv4i_4i(r5))
        r6 = F.relu(c6)
        mp3, idx4i = self.mp(r6)  # 3rd max-pooling

        c7 = self.batchNorm_8i_1(self.conv4i_8i(mp3))  # Lowest resolution
        r7 = F.relu(c7)
        c8 = self.batchNorm_8i_2(self.conv8i_8i(r7))
        r8 = F.relu(c8)

        c9 = self.conv8i_1(r8)
        r9 = F.relu(c9)

        shape_fc = list(r9.shape)  # Shape before the converting the tensor to a two dimensional array

        f0 = r9.view(o_shape[0], -1)  # HC-1
        f1 = self.fc1(f0)  # Este seria el "code"

        return f1, shape_fc

    def decode(self, code, shape_fc):
        f2 = self.fc2(code)  # HC-2
        r7 = F.relu(f2)
        r7_2 = r7.view(shape_fc)

        uc1 = self.upconv1_8i(r7_2)  # 1st upconvolution
        c11 = self.batchNorm_4i_5(self.conv8i_4i(uc1))
        r11 = F.relu(c11)
        c12 = self.batchNorm_4i_6(self.conv4i_4i(r11))
        r12 = F.relu(c12)

        uc3 = self.upconv4i_4i(r12)  # 3rd upconvolution
        c15 = self.batchNorm_2i_3(self.conv4i_2i(uc3))
        r15 = F.relu(c15)
        c16 = self.batchNorm_2i_4(self.conv2i_2i(r15))
        r16 = F.relu(c16)

        uc4 = self.upconv2i_2i(r16)  # 4th upconvolution
        c17 = self.batchNorm_i_3(self.conv2i_i(uc4))
        r17 = F.relu(c17)
        c18 = self.batchNorm_i_4(self.convi_i(r17))
        r18 = F.relu(c18)
        c19 = self.convi_2(r18)

        return F.softmax(sigmoid(c19), dim=1)

    def forward(self, x):
        code, shape_fc = self.encode(x)
        result = self.decode(code, shape_fc)

        return result
