# deep-brain-extractor

A couple of PyTorch based models for performing brain segmentation or skull reconstruction on craniectomy CT images.

The implemented convolutional neural networks are based on the [U-Net](https://arxiv.org/abs/1505.04597) model,
which was adapted to these particular problems.

## The repo

### Requirements

* Python 3.7
* [Anaconda](https://www.anaconda.com/products/individual)

### Installation

#### Clone the repo

The first step is to clone this repo, using git:

```sh
git clone https://gitlab.com/matzkin/deep-brain-extractor.git
```

#### Installing SimpleElastix

If you are using the here provided Preprocessing, you should install SimpleElastix (a SimpleITK extension) since it's required for registration. We've included a page in the [project wiki](https://gitlab.com/matzkin/deep-brain-extractor/-/wikis/Installing-SimpleElastix) with further details. Also you can follow [this unofficial tutorial for python 3](https://github.com/rcasero/pysto/wiki/Build-and-install-SimpleElastix-for-python-3.x) or read the [official docs](https://simpleelastix.readthedocs.io) (may be outdated). 

#### Create the enviroment 
Next, set up the conda enviroment doing:

```sh
conda create --name venvAEs python==3.7
```

A conda enviroment called *venvAEs* will be created. Now you can activate it via:

```sh
conda activate venvAEs
```

After this, install the required packages listed in environment_root.yml, doing

```sh
conda env update --name venvAEs --file environment_root.yml 
```

## Making it work

### Prepare your data

Since the dataset is not included, you must use your own for training a model.

This is meant for the cases in which we have both the CT scan images and their corresponding brain masks. If you use the preprocessing methods, you are expected to have all the files in one folder, having an identifier for the image files, such as subj001_**image**.nii.gz and an identifier for the mask for the same subject subj001_**head_mask**.nii.gz. You can preprocess images without mask.

### Preprocessing
<img src="assets/preprocessing-example.png" width="500">

You can preprocess the data for both the segmentation and reconstruction process, in this case an example of the latter will be shown. For processing the img/train folder, with the identifiers image and head_mask, run:


```sh
python Preprocessor.py -r -f img/train -i image -m head_mask
```

Once you have preprocessed your files, a csv file will appear in the output folder (by default is a subfolder named 'preproccesed' of the inpunt folder). You can partition that file in three different files for creating the train / validation / test splits. You'll need these csv files for train or predict. 

You can check the help via:


```sh
python Preprocessor.py -h
```

### Training

There are five different models that could be trained, each of which associated with a file in the cfg folder:

#### Segmentation purposes
<img src="assets/brain-segmentation.png" width="500">

* U-Net for image segmentation (unet.cfg).
* [ACNN](https://arxiv.org/abs/1705.08302) for learning an AE regularizer for helping out segmentation training (acnn.cfg).

#### Flap reconstruction purposes

<img src="assets/skull-rec.png" width="500">

* U-Net for full skull reconstruction (skullrec.cfg).
* AE for skull reconstruction (skullrec_ae.cfg).
* U-Net for flap only reconstruction (flaprec.cfg). 


In any case, you should edit the corresponding cfg file with a text editor for establishing the train parameters or if you don't want to do so you can set the train flag to false for predicting using the trained model and test data. A description of the cfg file is in the [project wiki](https://gitlab.com/matzkin/deep-brain-extractor/-/wikis/Understanding-cfg-files).

Once configured the file, you should run:

```sh
python trainTestModel.py
``` 

with the corresponding argument. For example: 


```sh
python trainTestModel.py -f
``` 

Will load the flaprec.cfg configuration file and train/predict if the corresponding flags are set to true. See `--help` for more details.

## Citing our work

If you have found this repo useful, please consider citing our work

```
@misc{SkullMICCAI20,
    author = {Matzkin Franco, Ferrante Enzo},
    title = {Self-supervised Skull Reconstruction in Brain CT Images with Decompressive Craniectomy},
    howpublished = {\url{https://arxiv.com}},
    year = {2020}
}
```

Project logo icons made by Pixel perfect from [www.flaticon.com](www.flaticon.com)