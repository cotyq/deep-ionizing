# In this file I'll put the PyTorch Dataset definitions, needed for proper loading of images.
# It's recommended to declare data augmentation here, using inheritance and keeping a simple instancing interface.

import os

import SimpleITK as sitk
import numpy as np
import pandas as pd
import torch
import torchvision
from torch.utils.data import Dataset

import common as cmf
from transforms import flap_rec_transform


def load_atlas_and_append_at_axis(atlas_path, image, axis=0):
    if os.path.exists(atlas_path):
        atlas_path = torch.tensor(sitk.GetArrayFromImage(sitk.ReadImage(atlas_path)),
                                  dtype=torch.float).unsqueeze(axis)
        image = torch.cat((image, atlas_path), axis)
    return image


class BrainSegmentationDataset(Dataset):
    """
    This dataset loads the CT scans and its masks
    """

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.segmentations_frame = pd.read_csv(csv_file, )
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.segmentations_frame)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.segmentations_frame.iloc[idx, 0])
        seg_name = os.path.join(self.root_dir, self.segmentations_frame.iloc[idx, 1])

        image = sitk.GetArrayFromImage(sitk.ReadImage(img_name))
        segmentation = sitk.GetArrayFromImage(sitk.ReadImage(seg_name)) if os.path.exists(seg_name) else image

        # image, padding = cmf.padVolumeToMakeItMultipleOf(image, [16, 16, 16], return_padding=True)
        # segmentation = cmf.padVolumeToMakeItMultipleOf(segmentation, [16, 16, 16])

        # image, padding = cmf.fixed_pad(image, [224, 512, 512], return_padding=True)
        # segmentation, _ = cmf.fixed_pad(segmentation, [224, 512, 512], return_padding=True)

        # padding = (0, 0, 0)
        image = torch.tensor(image, dtype=torch.float32).unsqueeze(0)
        target = cmf.one_hot_encoding(torch.tensor(segmentation, dtype=torch.float32).unsqueeze(0)).squeeze(0)
        sample = {'image': image,
                  'target': target,
                  'filepath': img_name}
        # sample = {'image': image, 'segmentation': segmentation, 'filepath': img_name,
        #           'direction': direction, 'origin': origin, 'padding': padding}

        return sample


class NiftiImageOnlyWithAtlas(Dataset):

    def __init__(self, csv_file, root_dir, transform=None, atlas_path='assets/atlas/reg/atlas_304_224.nii.gz'):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.segmentations_frame = pd.read_csv(csv_file, )
        self.root_dir = root_dir
        self.transform = transform
        self.atlas_path = atlas_path

    def __len__(self):
        return len(self.segmentations_frame)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.segmentations_frame.iloc[idx, 0])
        sample = {'image': torch.tensor(sitk.GetArrayFromImage(sitk.ReadImage(img_name)), dtype=torch.float),
                  'filepath': img_name}
        if self.transform:
            return self.transform(sample)
        sample['image'] = sample['image'].unsqueeze(0)

        # It does nothing if atlas is None
        sample['image'] = load_atlas_and_append_at_axis(self.atlas_path, sample['image'], 0)

        return sample


class NiftiImageOnly(NiftiImageOnlyWithAtlas):
    """
    This dataset loads the CT scans only
    """
    def __init__(self, csv_file, root_dir, transform=None):
        super(NiftiImageOnly, self).__init__(csv_file, root_dir, transform, atlas_path='')


class FlapRecTrainDataset(NiftiImageOnly):
    def __init__(self, csv_file, root_dir):
        super(FlapRecTrainDataset, self).__init__(csv_file, root_dir, flap_rec_transform)


class SelectiveFRDataset(NiftiImageOnly):
    def __init__(self, csv_file, root_dir, full_skull_file_identifier='complete_skull',
                 fr_transform=flap_rec_transform,
                 atlas_path='assets/atlas/reg/atlas_304_224.nii.gz'):
        super(SelectiveFRDataset, self).__init__(csv_file, root_dir)
        self.full_skull_file_identifier = full_skull_file_identifier
        self.transform = fr_transform
        self.atlas_path = atlas_path

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.segmentations_frame.iloc[idx, 0])

        # We will apply the transform based on the file path
        image = torch.tensor(sitk.GetArrayFromImage(sitk.ReadImage(img_name)), dtype=torch.float)

        if self.full_skull_file_identifier in img_name:  # In this case I'll always apply the flap extraction
            sample = {'image': image, 'filepath': img_name}
            sample = self.transform(sample)
        else:  # The flap is already extracted
            seg_name = os.path.join(self.root_dir, self.segmentations_frame.iloc[idx, 1])
            segmentation = sitk.GetArrayFromImage(sitk.ReadImage(seg_name)) if os.path.exists(seg_name) else image

            image = image.unsqueeze(0)
            target = cmf.one_hot_encoding(torch.tensor(segmentation, dtype=torch.float32).unsqueeze(0)).squeeze(0)
            sample = {'image': image, 'target': target, 'filepath': img_name}

        sample['image'] = load_atlas_and_append_at_axis(self.atlas_path, sample['image'], 0)

        return sample