# This file contains some used functions for manipulating images

import common as cmf
import SimpleITK as sitk


def compute_difference():
    sitk_complete = sitk.ReadImage('../data/all/clip90-91_sp2-2-2-rigid/cr_gt/1080_21915_image.nii.gz')
    sitk_craniectomy = sitk.ReadImage('../data/all/clip90-91_sp2-2-2-rigid/cr_gt/1080_23915_image.nii.gz')
    out_path = '../data/all/clip90-91_sp2-2-2-rigid/cr_gt/1080_diff.nii.gz'

    diff = cmf.diff_sitk(sitk_craniectomy, sitk_complete)
    sitk.WriteImage(diff, out_path)


def get_boxplots():
    """
    Simple script for loading csv stored metrics (dice, HD, Avg Surface Dist) of a set of models for comparing them with
    a matplotlib boxplot.
    :return:
    """
    statsPaths = ['../data/all/clip90-91_sp2-2-2-rigid/cr_gt/othernames/pred_a_CR_20190911_2000_1e-4_bce_bz9/stats.txt',
                  '../data/normal/clip90-91_sp2-2-2-rigid/pred_a_CR_20190911_2000_1e-4_bce_bz9/stats.txt']

    methods = ['real craniectomy', 'simulated craniectomy']

    stat_dict = cmf.loadStats(statsPaths, methods)
    cmf.boxplots(stat_dict)


if __name__ == '__main__':
    compute_difference()
    # get_boxplots()
