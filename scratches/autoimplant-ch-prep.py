# Some functions used for the autoimplant challenge
import os
import subprocess
from multiprocessing import Pool

import SimpleITK as sitk

import common as cmf
from Preprocessor import preprocess_ct_files


def reverse_registration(orig_img, registered_img, par_map_s, reg_file):
    unreg_sitk = sitk.ReadImage(orig_img)
    invreg_filter = sitk.ElastixImageFilter()

    # For the inverse transform calculation we need to set the original fixed image as both fixed and moving image
    # (section 6.1.6 of the elastix manual)
    registered_sitk = sitk.ReadImage(registered_img)
    invreg_filter.SetFixedImage(registered_sitk)
    invreg_filter.SetMovingImage(registered_sitk)

    # Take the parameter map of the previous registration filter_name
    # par_map = sitk.GetDefaultParameterMap('rigid')
    par_map = sitk.ReadParameterFile(par_map_s) if os.path.exists(par_map_s) else sitk.GetDefaultParameterMap(par_map_s)

    # This metric_str makes elastix find the inverse.
    par_map['Metric'] = ['DisplacementMagnitudePenalty']
    par_map['InitialTransformParametersFileName'] = [reg_file]
    par_map['HowToCombineTransforms'] = ['Compose']

    par_map['Size'] = [str(f) for f in unreg_sitk.GetSize()]
    par_map['Spacing'] = [str(f) for f in unreg_sitk.GetSpacing()]
    par_map['Origin'] = [str(f) for f in unreg_sitk.GetOrigin()]
    par_map['Direction'] = [str(f) for f in unreg_sitk.GetDirection()]
    par_map['DefaultPixelValue'] = [str(0)]
    par_map["ResampleInterpolator"] = ["FinalNearestNeighborInterpolator"]

    invreg_filter.SetParameterMap(par_map)

    # invreg_filter.LogToConsoleOn()

    res_img = invreg_filter.Execute()  # Get the inverse transform
    sitk.Show(res_img)
    print("success.")


def reverse_registration_folder(prep_folder, atlas_path, param_path, image_ext='.nrrd'):
    for file in os.listdir(prep_folder):
        if file.endswith(image_ext):
            reg_file = os.path.join(pred_folder, file + '_reg.txt')
            if os.path.exists(reg_file):
                # orig_img = sitk.ReadImage(os.path.join(prep_folder.replace('/prep', ''), file))  # Original image
                # reg_image = sitk.ReadImage(os.path.join(prep_folder, file))  # Registered image

                reverse_registration(os.path.join(prep_folder.replace('/prep', ''), file),
                                     os.path.join(prep_folder, file),
                                     param_path,
                                     reg_file)  # Inverse transform parametermap

                #
                # elastix_image_filter = sitk.ElastixImageFilter()
                #
                # param_map_1 = sitk.ReadParameterFile(reg_file)  # Parametermap used for registering
                #
                # param_map_2["ResampleInterpolator"] = ["FinalNearestNeighborInterpolator"]  # Switch to Nearest Neighbor
                # param_map_2["DefaultPixelValue"] = ['0']  # New pixels will be zeros.
                # param_map_2["InitialTransformParametersFileName"] = ["NoInitialTransform"]
                #
                # param_map_2['Size'] = [str(f) for f in list(orig_img.GetSize())]
                # param_map_2['Spacing'] = [str(f) for f in orig_img.GetSpacing()]
                # param_map_2['Origin'] = [str(f) for f in orig_img.GetOrigin()]
                # param_map_2['Direction'] = [str(f) for f in orig_img.GetDirection()]
                #
                # elastix_image_filter.LogToConsoleOn()
                # elastix_image_filter.SetParameterMap(param_map_1)
                # elastix_image_filter.AddParameterMap(param_map_2)
                #
                # elastix_image_filter.SetFixedImage(reg_image)
                # elastix_image_filter.SetMovingImage(reg_image)
                # unreg_image = elastix_image_filter.Execute()
                #
                # # unreg_image = sitk.Transformix(reg_image, inv_param_map)
                # sitk.WriteImage(unreg_image, os.path.join(prep_folder, file.replace(image_ext, '_ur'+image_ext)))
                #
                #
                # # # Manually set the basic image parameters TODO continue here
                # # inv_par_map['Size'] = [str(f) for f in list(original_size)]
                # # inv_par_map['Spacing'] = [str(f) for f in list(original_spacing)]
                # # inv_par_map['Origin'] = [str(f) for f in list(original_origin)]
                # # inv_par_map['Direction'] = [str(f) for f in list(original_direction)]
                # # inv_par_map['Direction'] = [str(f) for f in list(original_direction)]
                # # inv_par_map['DefaultPixelValue'] = [str(-1000)]
                # #
                # # inv_par_map["InitialTransformParametersFileName"] = ["NoInitialTransform"]  # As mentioned in the elastix manual
                # #
                # # # Save the inverse transform
                # # reg_param_file = os.path.join(self.save_path, os.path.split(self.f_name)[1] + "_invreg.txt")
                # # reg_filter.WriteParameterFile(inv_par_map, reg_param_file)
                #


def change_img_ext_folder_sitk(ch_path, in_ext, out_ext, out_folder='ext_renamed'):
    """  Change image extensions from a folder using SimpleITK. This function simply opens and saves the files in the
        provided folder with the required extension.

    :param ch_path: Path of the folder containing the images.
    :param in_ext: Extension to be replaced.
    :param out_ext: New extension.
    :return:
    """
    print("[{} -> {}] Folder: {}".format(in_ext, out_ext, ch_path))
    cmf.veri_folder(os.path.join(ch_path, out_folder))
    for file in os.listdir(ch_path):
        if file.endswith(in_ext):
            print("    File: {}...".format(file), end='')
            f_path = os.path.join(ch_path, file)
            o_path = os.path.join(ch_path, out_folder, file).replace(in_ext, out_ext)
            sitk_img = sitk.ReadImage(f_path, sitk.sitkUInt8)
            sitk.WriteImage(sitk_img, o_path)
            print(" saved in {}.".format(o_path))


def register_affine_nneigh(moving_image, fixed_image, out_image, mat_path, return_command=False, transformation='rigid',
                           interp='nearestneighbour'):
    dof = 6 if transformation == 'rigid' else 9  # 9 is affine

    if os.path.exists(out_image):
        print("already exists (skipping).")
        return

    command = 'flirt -in {} ' \
              '-ref {} ' \
              '-out {} ' \
              '-omat {} ' \
              '-bins 256 ' \
              '-cost corratio ' \
              '-searchrx -90 90 ' \
              '-searchry -90 90 ' \
              '-searchrz -90 90 ' \
              '-dof {}  ' \
              '-interp {}'.format(moving_image, fixed_image, out_image, mat_path, dof, interp)
    if return_command:
        return command

    os.environ['OMP_NUM_THREADS'] = '10'
    os.system(command)
    print(" saved in {}.".format(out_image))


def apply_mat_affine_nneigh(moving_image, out_image, mat_path, reference, return_command=False):
    if os.path.exists(out_image):
        print("already exists (skipping).")
        return

    command = '/usr/local/fsl/bin/flirt ' \
              '-in {} ' \
              '-applyxfm -init {} ' \
              '-out {} ' \
              '-paddingsize 0.0 ' \
              '-interp nearestneighbour ' \
              '-ref {}'.format(moving_image, mat_path, out_image, reference)
    if return_command:
        return command

    os.environ['OMP_NUM_THREADS'] = '10'
    os.system(command)
    print(" saved in {}.".format(out_image))


def register_folder_flirt(folder, fixed_image, out_folder='reg', ext=".nii.gz", transformation='rigid',
                          interp='nearestneighbour'):
    print("[Registering images to atlas] Folder: {}".format(folder))
    cmf.veri_folder(os.path.join(folder, out_folder))

    commands = []
    for file in os.listdir(folder):
        if file.endswith(ext):
            print("    File: {}...".format(file), end='')
            moving_image = os.path.join(folder, file)
            out_image = os.path.join(folder, out_folder, file)
            out_mat = os.path.join(folder, out_folder, file.replace(ext, '.mat'))

            command = register_affine_nneigh(moving_image, fixed_image, out_image, out_mat, True, transformation,
                                             interp)
            commands.append(command) if command else 0

    process_pool = Pool(processes=8)
    process_pool.map(execute, commands)


def register_flaps(images_folder, flaps_folder, out_folder='reg', ext=".nii.gz"):
    print("[Applying .map transforms to folder] Folder: {}".format(flaps_folder))
    cmf.veri_folder(os.path.join(flaps_folder, out_folder))

    commands = []
    for file in os.listdir(images_folder):
        if file.endswith(ext):
            print("    File: {}...".format(file), end='')
            moving_image = os.path.join(flaps_folder, file)
            reference = os.path.join(images_folder, file)
            out_image = os.path.join(flaps_folder, out_folder, file)
            mat_file = os.path.join(images_folder, file.replace(ext, '.mat'))

            command = apply_mat_affine_nneigh(moving_image, out_image, mat_file, reference, return_command=True)
            commands.append(command) if command else 0

    process_pool = Pool(processes=8)
    process_pool.map(execute, commands)


def execute(process):
    os.system(process)


if __name__ == '__main__':
    base_folder = '/home/franco/pc10'
    # base_folder = '/home/fmatzkin'

    # Complete skull
    input_ff = '../datasets/autoimplant-challenge/training_set/defective_skull'
    out_folder = '../datasets/autoimplant-challenge/training_set/complete_skull/preptest'

    # Defective skull
    input_ff_defect = '../datasets/autoimplant-challenge/training_set/defective_skull/selected'
    out_folder_defect = '../datasets/autoimplant-challenge/training_set/defective_skull/selected/prep2mm'

    atlas_path = 'assets/atlas'  # Original CT and brain mask atlas path
    reg_atlas = '../Atlas3/reg/atlas_skull_1mm.nii.gz'  # Thresholded CT atlas
    reg_atlas_half_mm = '../Atlas3/reg/atlas_skull_halfmm.nii.gz'
    reg_atlas2mm = '../Atlas3/reg/atlas_skull_2mm.nii.gz'  # Thresholded CT atlas
    reg_atlas_400208 = base_folder + '/Code/deep-brain-extractor/assets/atlas/reg/atlas_skull_400x400x208.nii.gz'
    reg_atlas_512_224 = base_folder + '/Code/deep-brain-extractor/assets/atlas/reg/atlas_skull_512_224.nii.gz'
    reg_atlas_304_224 = base_folder + '/Code/deep-brain-extractor/assets/atlas/reg/atlas_304_224.nii.gz'

    test_img = base_folder + '/Code/datasets/autoimplant-challenge/training_set/defective_skull/ext_renamed/test'

    # -------------
    # PRINT THE BIGGEST DIMENSIONS IN EACH COORD (FOR THE SELECTED FOLDER)
    # print(cmf.get_max_dims(input_ff, ext=".nrrd"))

    # -------------
    # Convert nrrd to .nii.gz
    # ch_path = base_folder + '/Code/datasets/autoimplant-challenge/training_set/implant'
    # change_img_ext_folder_sitk(ch_path, in_ext='.nrrd', out_ext='.nii.gz')

    # -------------
    # CREATE CSV FILE
    # folder ='/home/franco/pc10/Code/datasets/autoimplant-challenge/additional_test_set_for_participants/ext_renamed/prep_304_224'
    # cmf.folder_to_csv(folder, image_extension='.nii.gz')  # List the filenames into a csv file
    # -------------

    # PREPROCESS THE ATLAS (CONVERT TO BONE SEGMENTATION)
    # out_atlas = os.path.join(atlas_path, 'prepAImp')
    # preprocess_ct_files(atlas_path, out_atlas, clip_intensity_values=[150, 151], threshold=True, image_identifier='',
    #                     mask_identifier='', target_spacing=[.695, .695, .715], overwrite_files=True)
    # -------------

    # PREPROCESS A FOLDER

    # preprocess_ct_files(input_ff, out_folder,
    #                     register=False, atlas_path=reg_atlas2mm, binary=True,
    #                     target_spacing=[1, 1, 1],
    #                     overwrite_files=False, include_subfolders=False, image_extension=".nrrd",
    #                     random_blank_patch=False, image_identifier='', mask_identifier=None)

    # -------------
    # complete_imgs_nii = base_folder + '/Code/datasets/autoimplant-challenge/training_set/complete_skull/ext_renamed'
    # defective_imgs_nii = base_folder + '/Code/datasets/autoimplant-challenge/training_set/defective_skull/ext_renamed'
    # test_1 = base_folder + '/Code/datasets/autoimplant-challenge/test_set_for_participants/ext_renamed'
    # test_2 = base_folder + '/Code/datasets/autoimplant-challenge/additional_test_set_for_participants/ext_renamed'
    # register_folder_flirt(complete_imgs_nii, reg_atlas_304_224, 'prep_304_224')
    # register_folder_flirt(test_1, reg_atlas_304_224, 'prep_304_224')
    # register_folder_flirt(test_2, reg_atlas_304_224, 'prep_304_224')
    # register_folder_flirt(defective_imgs_nii, reg_atlas_304_224, 'prep_304_224')

    fail_imgs = '/home/franco/pc10/Code/datasets/autoimplant-challenge/additional_test_set_for_participants/ext_renamed/fail'
    reg_atlas_304_224_hole = '/home/franco/pc10/Code/datasets/autoimplant-challenge/test_set_for_participants/ext_renamed/prep_304_224/002.nii.gz'
    register_folder_flirt(fail_imgs, reg_atlas_304_224_hole, 'prep_304_224_fail', transformation='rigid')

    # --------------
    # APPLY .MATs FROM ONE FOLDER TO OTHER
    # images_folder = base_folder + '/Code/datasets/autoimplant-challenge/training_set/defective_skull/ext_renamed/prep_304_224'
    # flaps_folder = base_folder + '/Code/datasets/autoimplant-challenge/training_set/implant/ext_renamed'
    # register_flaps(images_folder, flaps_folder, 'prep_304_224')

    # -------------
    # REVERSE THE REGISTRATION
    # atlas_path = '../Atlas3/prep/atlas3_nonrigid_masked_1mm.nii.gz'
    # param_path = 'translation'
    # pred_folder = out_folder
    # reverse_registration_folder(pred_folder, atlas_path, param_path, '.nrrd')
    # -------------
